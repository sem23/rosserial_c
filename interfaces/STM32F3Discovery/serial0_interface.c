/* ROS */
#include "ros.h"
/* hardware */
#include "hw_config.h"

/* defines buffer size */
#define SERIAL0_BUFFER_SIZE 1024
/* defines buffer mask */
#define SERIAL0_BUFFER_MASK (SERIAL0_BUFFER_SIZE-1)

/* serial0 buffers */
uint8_t serial0_buffer[SERIAL0_BUFFER_SIZE];

rosReturnCode_t serial0FifoWrite(uint8_t* byte);
rosReturnCode_t serial0FifoRead(uint8_t* byte);

rosFifoBuffer_t serial0_fifo =
{
  .data = serial0_buffer,
  .read_index = 0,
  .write_index = 0,
  .read = serial0FifoRead,
  .write = serial0FifoWrite,
};

rosReturnCode_t serial0FifoWrite(uint8_t* byte)
{
  uint8_t next = ((serial0_fifo.write_index + 1) & SERIAL0_BUFFER_MASK);
  if (serial0_fifo.read_index == next)
    return ROS_FIFO_BUFFER_OVERFLOW;
  serial0_fifo.data[serial0_fifo.write_index & SERIAL0_BUFFER_MASK] = *byte;
  serial0_fifo.write_index = next;
  return ROS_SUCCESS;
}

rosReturnCode_t serial0FifoRead(uint8_t* byte)
{
  if (serial0_fifo.read_index == serial0_fifo.write_index)
    return ROS_FIFO_BUFFER_EMPTY;
  *byte = serial0_fifo.data[serial0_fifo.read_index];
  serial0_fifo.read_index = (serial0_fifo.read_index+1) & SERIAL0_BUFFER_MASK;
  return ROS_SUCCESS;
}

rosReturnCode_t serial0ReceiveByte(uint8_t* byte)
{
  /* read byte from fifo buffer */
  return serial0_fifo.read(byte);
}

rosReturnCode_t serial0SendByte(uint8_t* byte)
{
  /* send byte via USART1 */
  USART_SendData(USART1, *byte);
  while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
  return ROS_SUCCESS;
}

rosInterface_t serial0_interface =
{
  .device_id = "serial0",
  .send = serial0SendByte,
  .receive = serial0ReceiveByte,
//  .package_oriented = false,
  .interface_type = MASTER,
};

/* interface corresponding spinner */
rosSpinner_t serial0_spinner =
{
  .spin = masterSpinner,
};

/* interface corresponding nodehandle */
rosNodeHandle_t serial0_nodehandle =
{
  .subscribe = rosSubscribe,
  .advertise = rosAdvertise,
  .interface = &serial0_interface,
  .spinner   = &serial0_spinner,
  .ok        = ROS_OK,
};
