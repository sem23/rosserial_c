/* ROS */
#include "ros.h"
/* hardware */
#include "hw_config.h"

extern uint8_t USB_Buffer[USB_DATA_SIZE];
extern uint32_t USB_ptr_in;
extern uint32_t USB_ptr_out;
extern uint32_t USB_length;

/* defines buffer size */
#define USB_VCP_BUFFER_SIZE 2048
/* defines buffer mask */
#define USB_VCP_BUFFER_MASK (USB_VCP_BUFFER_SIZE-1)
/* usb_vcp buffer */
uint8_t usb_vcp_buffer[USB_VCP_BUFFER_SIZE];

rosReturnCode_t usb_vcpFifoWrite(uint8_t* byte);
rosReturnCode_t usb_vcpFifoRead(uint8_t* byte);

rosFifoBuffer_t usb_vcp_fifo =
{
  .data = usb_vcp_buffer,
  .read_index = 0,
  .write_index = 0,
  .read = usb_vcpFifoRead,
  .write = usb_vcpFifoWrite,
};

rosReturnCode_t usb_vcpFifoWrite(uint8_t* byte)
{
  uint8_t next = ((usb_vcp_fifo.write_index + 1) & USB_VCP_BUFFER_MASK);
  if (usb_vcp_fifo.read_index == next)
    return ROS_FIFO_BUFFER_OVERFLOW;
  usb_vcp_fifo.data[usb_vcp_fifo.write_index & USB_VCP_BUFFER_MASK] = *byte;
  usb_vcp_fifo.write_index = next;
  return ROS_SUCCESS;
}

rosReturnCode_t usb_vcpFifoRead(uint8_t* byte)
{
  if (usb_vcp_fifo.read_index == usb_vcp_fifo.write_index)
    return ROS_FIFO_BUFFER_EMPTY;
  *byte = usb_vcp_fifo.data[usb_vcp_fifo.read_index];
  usb_vcp_fifo.read_index = (usb_vcp_fifo.read_index+1) & USB_VCP_BUFFER_MASK;
  return ROS_SUCCESS;
}

rosReturnCode_t usb_vcpReceiveByte(uint8_t* byte)
{
  /* read byte from fifo buffer */
  return usb_vcp_fifo.read(byte);
}

rosReturnCode_t usb_vcpSendByte(uint8_t* byte)
{
  USB_Buffer[USB_ptr_in] = *byte;
  USB_ptr_in++;

  /* To avoid buffer overflow */
  if (USB_ptr_in == USB_DATA_SIZE)
  {
    USB_ptr_in = 0;
  }
  return ROS_SUCCESS;
}

rosInterface_t usb_vcp_interface =
{
  .device_id = "usb_vcp",
  .send = usb_vcpSendByte,
  .receive = usb_vcpReceiveByte,
//  .package_oriented = false,
  .interface_type = SLAVE,
};

/* interface corresponding spinner */
rosSpinner_t usb_vcp_spinner =
{
  .spin = slaveSpinner,
};

/* interface corresponding nodehandle */
rosNodeHandle_t usb_vcp_nodehandle =
{
  .subscribe = rosSubscribe,
  .advertise = rosAdvertise,
  .interface = &usb_vcp_interface,
  .spinner   = &usb_vcp_spinner,
  .ok        = ROS_OK,
};
