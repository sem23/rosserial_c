/* ROS */
#include "ros.h"
/* imu */
#include "imu.h"
/* ROS messages */
#include "ImuC.h"

void imuNode(void *pvParameters)
{
  /* initialize IMU */
  imuInit();
  /* get pointer to global nodehandle */
  rosNodeHandle_t* nh = rosNodeHandle("serial0");
  /* setup ROS IMU message */
  Imu_t imu_msg = rosCreateImu_t(nh);
  /* advertise imu msg */
  rosPublisher_t *imu_pub = nh->advertise(&imu_msg.mh, "/imu", 512);
  /* set frame id */
  imu_msg.header.frame_id = "/base_link";
  /* setup sensor covariances */
  imu_msg.angular_velocity_covariance[0] = 1e-3f;
  imu_msg.angular_velocity_covariance[1] = 0.0f;
  imu_msg.angular_velocity_covariance[2] = 0.0f;
  imu_msg.angular_velocity_covariance[3] = 0.0f;
  imu_msg.angular_velocity_covariance[4] = 1e-3f;
  imu_msg.angular_velocity_covariance[5] = 0.0f;
  imu_msg.angular_velocity_covariance[6] = 0.0f;
  imu_msg.angular_velocity_covariance[7] = 0.0f;
  imu_msg.angular_velocity_covariance[8] = 1e-3f;

  imu_msg.linear_acceleration_covariance[0] = 1e-3f;
  imu_msg.linear_acceleration_covariance[1] = 0.0f;
  imu_msg.linear_acceleration_covariance[2] = 0.0f;
  imu_msg.linear_acceleration_covariance[3] = 0.0f;
  imu_msg.linear_acceleration_covariance[4] = 1e-3f;
  imu_msg.linear_acceleration_covariance[5] = 0.0f;
  imu_msg.linear_acceleration_covariance[6] = 0.0f;
  imu_msg.linear_acceleration_covariance[7] = 0.0f;
  imu_msg.linear_acceleration_covariance[8] = 1e-3f;

  imu_msg.orientation_covariance[0] = 1e-3f;
  imu_msg.orientation_covariance[1] = 0.0f;
  imu_msg.orientation_covariance[2] = 0.0f;
  imu_msg.orientation_covariance[3] = 0.0f;
  imu_msg.orientation_covariance[4] = 1e-3f;
  imu_msg.orientation_covariance[5] = 0.0f;
  imu_msg.orientation_covariance[6] = 0.0f;
  imu_msg.orientation_covariance[7] = 0.0f;
  imu_msg.orientation_covariance[8] = 1e-3f;

  rosRate_t rate;
  rosRateInit(&rate, 20);

  while (rosOk(nh))
  {
    /* update IMU data */
    imuUpdate();
    /* set data to imu message */
    float* acc = imuGetAcc();
    float* mag = imuGetMag();
    float* gyr = imuGetGyr();
    float* ori = imuGetOrientation();
    imu_msg.angular_velocity.x = gyr[X];
    imu_msg.angular_velocity.y = gyr[Y];
    imu_msg.angular_velocity.z = gyr[Z];
    imu_msg.linear_acceleration.x = acc[X];
    imu_msg.linear_acceleration.y = acc[Y];
    imu_msg.linear_acceleration.z = acc[Z];
    imu_msg.orientation.w = ori[W];
    imu_msg.orientation.x = ori[X];
    imu_msg.orientation.y = ori[Y];
    imu_msg.orientation.z = ori[Z];
    /* set timestamp */
    imu_msg.header.stamp = rosTimeNow();
    /* publish message */
    imu_pub->publish(&imu_msg);
    /* sleep a while */
    rosRateSleep(&rate);
  }
} /* imuNode */