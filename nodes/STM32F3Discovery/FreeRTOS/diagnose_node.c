/* ROS */
#include <stdio.h>
#include "ros_freertos_nodes.h"
#include "ros.h"
#include "DiagnosticArrayC.h"

// TODO parse runtime stats: "0x0D,0x0A,nodename,0x09,0x09,slots,0x09,0x09,usage" 
const char g_runtime_stats[1024];

#define NUM_TASKS 6

KeyValue_t node_values[NUM_TASKS][1];
DiagnosticStatus_t node_status[NUM_TASKS];

static const char * rc_str_running = "running";
static const char * rc_str_hardware = "STM32F3";
static const char * rc_str_usage = "CPU Usage(%)";
char rc_buf_node_name[NUM_TASKS][15];
char rc_buf_node_values[NUM_TASKS][5];

void parseRuntimeStats()
{
  int i = 0;
  char* current_char = g_runtime_stats;
  int dummy = 0;
  while(i < NUM_TASKS)
  {
    if(*current_char == '\r')
    {
      sscanf(current_char,"\r\n%s\t\t%d\t\t%s",rc_buf_node_name[i],&dummy,rc_buf_node_values[i]);
      ++i;
    }
    /* increment pointer to next char */
    current_char++;
  }
}

void diagnoseNode(void* args)
{ 
  int i;
  for (i = 0; i < NUM_TASKS; ++i)
  {
    node_values[i][0].key = rc_str_usage;
    node_values[i][0].value = rc_buf_node_values[i];

    node_status[i].level = 0;
    node_status[i].name = rc_buf_node_name[i];
    node_status[i].message = rc_str_running;
    node_status[i].hardware_id = rc_str_hardware;
    node_status[i].values = node_values[i];
    node_status[i].values_length = lengthof(node_values[i]);
  }

  /* get pointer to nodehandle */
  rosNodeHandle_t* nh = rosNodeHandle("serial0");
  
  DiagnosticArray_t diag_msg = rosCreateDiagnosticArray_t(nh);
  /* advertise diagnostic msg */
  rosPublisher_t *diag_pub = nh->advertise(&diag_msg.mh, "/diag", 1024);
  /* set frame id */
  diag_msg.header.frame_id = "/base_link";
  
  diag_msg.status = node_status;
  diag_msg.status_length = lengthof(node_status);

  rosRate_t rate;
  rosRateInit(&rate, 1);

  for(;;) 
  {
    /* get runtime stats */
    vTaskGetRunTimeStats(g_runtime_stats);

    parseRuntimeStats();

    /* set timestamp */
    diag_msg.header.stamp = rosTimeNow();

    diag_pub->publish(&diag_msg);
    
    rosRateSleep(&rate);
  }
}