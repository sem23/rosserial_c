/* ROS includes */
#include "ros.h"
/* Header(s) where nodefunctions are defined.
   You have to create the header yourself! */
#include "nodes.h"

/* make nodehandles available */
extern rosNodeHandle_t example0_nodehandle;
extern rosNodeHandle_t example1_nodehandle;

/* Add nodehandles to global list.
   Now they can be accessed from 
   everywhere via their Interface name */
rosNodeHandle_t* g_nodehandles[] =
{
  &example0_nodehandle,
  &example1_nodehandle,
};
/* set up size */
int g_nodehandles_size = lengthof(g_nodehandles);

/* list with nodes to start with OS */
osROSNode_t g_nodes[] =
{
  /* an example node */
  {
    .node_func = exampleNode,
    .name = "diagnose_node",
    .stack_size = 1024,
    .priority = 2,
    .args = NULL,
  },
  /* common spinner node polls all available nodehandle spinners */
  {
    .node_func = spinnerNode,
    .name = "spinner_node",
    .stack_size = 512,
    .priority = 1,
    .args = NULL,
  },
};
/* set up size */
uint8_t g_nodes_size = lengthof(g_nodes);
