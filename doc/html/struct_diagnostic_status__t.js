var struct_diagnostic_status__t =
[
    [ "hardware_id", "struct_diagnostic_status__t.html#a60e40fcc35ee094cf9a2c90e5a294004", null ],
    [ "hardware_id_length", "struct_diagnostic_status__t.html#aa92936ff022207b75fc7a2633fcfd67b", null ],
    [ "level", "struct_diagnostic_status__t.html#abbb6c7c49508c1ac374683f2d1159e08", null ],
    [ "message", "struct_diagnostic_status__t.html#a0b2e8c7f76df48129f994ecc46d5c66c", null ],
    [ "message_length", "struct_diagnostic_status__t.html#a38bb802e86683f0de88edc94b592319b", null ],
    [ "mh", "struct_diagnostic_status__t.html#a53d706092db9bb8f0ef8f2065850f906", null ],
    [ "name", "struct_diagnostic_status__t.html#a5ac083a645d964373f022d03df4849c8", null ],
    [ "name_length", "struct_diagnostic_status__t.html#ac07254ea6a3f8e384ee3f26c1c524f3a", null ],
    [ "values", "struct_diagnostic_status__t.html#a215571c9263e4fe88fa4513f59d38a1b", null ],
    [ "values_length", "struct_diagnostic_status__t.html#af703dcef4503c7a622f7baa609da1c82", null ]
];