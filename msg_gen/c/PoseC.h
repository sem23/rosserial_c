/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __POSE_H__
#define __POSE_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "PointC.h"
#include "QuaternionC.h"

typedef struct
{
  rosMessageHandle_t mh;
  Point_t position;
  Quaternion_t orientation;
} Pose_t;

static int bytesize_of_Pose_t(Pose_t* pose)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Point_t(&pose->position);
  bytesize+=bytesize_of_Quaternion_t(&pose->orientation);
  return bytesize;
}

static int serialize_Pose_t(Pose_t* pose, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Point_t(&pose->position, buffer, pos);
  pos+=serialize_Quaternion_t(&pose->orientation, buffer, pos);
  return (pos - start);
}

static int deserialize_Pose_t(Pose_t* pose, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Point_t(&pose->position, buffer, pos);
  pos+=deserialize_Quaternion_t(&pose->orientation, buffer, pos);
  return (pos - start);
}

static void delete_Pose_t(Pose_t* pose)
{
  delete_Point_t(&pose->position);
  delete_Quaternion_t(&pose->orientation);
}

static char* Pose_t_md5sum = "e45d45a5a1ce597b249e23fb30fc871f";

static Pose_t rosCreatePose_t(rosNodeHandle_t *nodehandle)
{
  Pose_t pose;
  pose.mh.nodehandle = nodehandle;
  pose.mh.type = "geometry_msgs/Pose";
  pose.mh.md5sum = Pose_t_md5sum;
  pose.mh.sizer = bytesize_of_Pose_t;
  pose.mh.serializer = serialize_Pose_t;
  pose.mh.deserializer = deserialize_Pose_t;
  return pose;
}

#endif
