/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __POSEWITHCOVARIANCE_H__
#define __POSEWITHCOVARIANCE_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "PoseC.h"

typedef struct
{
  rosMessageHandle_t mh;
  Pose_t pose;
  double covariance[36];
} PoseWithCovariance_t;

#ifndef __DOUBLE_ARRAY_SERIALIZERS__
#define __DOUBLE_ARRAY_SERIALIZERS__
static inline int serialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double)*length);
  return sizeof(double)*length;
}

static inline int deserialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double)*length);
  return sizeof(double)*length;
}
#endif

static int bytesize_of_PoseWithCovariance_t(PoseWithCovariance_t* posewithcovariance)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Pose_t(&posewithcovariance->pose);
  bytesize+=sizeof(double)*36;
  return bytesize;
}

static int serialize_PoseWithCovariance_t(PoseWithCovariance_t* posewithcovariance, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Pose_t(&posewithcovariance->pose, buffer, pos);
  pos+=serialize_double_array(&posewithcovariance->covariance[0], buffer, pos, 36);
  return (pos - start);
}

static int deserialize_PoseWithCovariance_t(PoseWithCovariance_t* posewithcovariance, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Pose_t(&posewithcovariance->pose, buffer, pos);
  pos+=deserialize_double_array(&posewithcovariance->covariance[0], buffer, pos, 36);
  return (pos - start);
}

static void delete_PoseWithCovariance_t(PoseWithCovariance_t* posewithcovariance)
{
  delete_Pose_t(&posewithcovariance->pose);
}

static char* PoseWithCovariance_t_md5sum = "c23e848cf1b7533a8d7c259073a97e6f";

static PoseWithCovariance_t rosCreatePoseWithCovariance_t(rosNodeHandle_t *nodehandle)
{
  PoseWithCovariance_t posewithcovariance;
  posewithcovariance.mh.nodehandle = nodehandle;
  posewithcovariance.mh.type = "geometry_msgs/PoseWithCovariance";
  posewithcovariance.mh.md5sum = PoseWithCovariance_t_md5sum;
  posewithcovariance.mh.sizer = bytesize_of_PoseWithCovariance_t;
  posewithcovariance.mh.serializer = serialize_PoseWithCovariance_t;
  posewithcovariance.mh.deserializer = deserialize_PoseWithCovariance_t;
  return posewithcovariance;
}

#endif