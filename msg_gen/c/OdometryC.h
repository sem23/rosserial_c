/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __ODOMETRY_H__
#define __ODOMETRY_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "HeaderC.h"
#include "PoseWithCovarianceC.h"
#include "TwistWithCovarianceC.h"

typedef struct
{
  rosMessageHandle_t mh;
  Header_t header;
  char* child_frame_id;
  int32_t child_frame_id_length;
  PoseWithCovariance_t pose;
  TwistWithCovariance_t twist;
} Odometry_t;

#ifndef __INT32_T_SERIALIZERS__
#define __INT32_T_SERIALIZERS__
static inline int serialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(int32_t));
  return sizeof(int32_t);
}

static inline int deserialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(int32_t));
  return sizeof(int32_t);
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_Odometry_t(Odometry_t* odometry)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Header_t(&odometry->header);
  bytesize+=sizeof(int32_t);
  odometry->child_frame_id_length = strlen(odometry->child_frame_id);
  bytesize+=sizeof(char)*odometry->child_frame_id_length;
  bytesize+=bytesize_of_PoseWithCovariance_t(&odometry->pose);
  bytesize+=bytesize_of_TwistWithCovariance_t(&odometry->twist);
  return bytesize;
}

static int serialize_Odometry_t(Odometry_t* odometry, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Header_t(&odometry->header, buffer, pos);
  odometry->child_frame_id_length = strlen(odometry->child_frame_id);
  pos+=serialize_int32_t(&odometry->child_frame_id_length, buffer, pos);
  pos+=serialize_char_array(&odometry->child_frame_id[0], buffer, pos, odometry->child_frame_id_length);
  pos+=serialize_PoseWithCovariance_t(&odometry->pose, buffer, pos);
  pos+=serialize_TwistWithCovariance_t(&odometry->twist, buffer, pos);
  return (pos - start);
}

static int deserialize_Odometry_t(Odometry_t* odometry, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Header_t(&odometry->header, buffer, pos);
  pos+=deserialize_int32_t(&odometry->child_frame_id_length, buffer, pos);
  odometry->child_frame_id = (char*)malloc(odometry->child_frame_id_length);
  pos+=deserialize_char_array(&odometry->child_frame_id[0], buffer, pos, odometry->child_frame_id_length);
  pos+=deserialize_PoseWithCovariance_t(&odometry->pose, buffer, pos);
  pos+=deserialize_TwistWithCovariance_t(&odometry->twist, buffer, pos);
  return (pos - start);
}

static void delete_Odometry_t(Odometry_t* odometry)
{
  delete_Header_t(&odometry->header);
  free(odometry->child_frame_id);
  delete_PoseWithCovariance_t(&odometry->pose);
  delete_TwistWithCovariance_t(&odometry->twist);
}

static char* Odometry_t_md5sum = "cd5e73d190d741a2f92e81eda573aca7";

static Odometry_t rosCreateOdometry_t(rosNodeHandle_t *nodehandle)
{
  Odometry_t odometry;
  odometry.mh.nodehandle = nodehandle;
  odometry.mh.type = "nav_msgs/Odometry";
  odometry.mh.md5sum = Odometry_t_md5sum;
  odometry.mh.sizer = bytesize_of_Odometry_t;
  odometry.mh.serializer = serialize_Odometry_t;
  odometry.mh.deserializer = deserialize_Odometry_t;
  return odometry;
}

#endif
