/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __TOPICINFO_H__
#define __TOPICINFO_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  uint16_t topic_id;
  char* topic_name;
  int32_t topic_name_length;
  char* message_type;
  int32_t message_type_length;
  char* md5sum;
  int32_t md5sum_length;
  int32_t buffer_size;
} TopicInfo_t;

#ifndef __UINT16_T_SERIALIZERS__
#define __UINT16_T_SERIALIZERS__
static inline int serialize_uint16_t(uint16_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(uint16_t));
  return sizeof(uint16_t);
}

static inline int deserialize_uint16_t(uint16_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(uint16_t));
  return sizeof(uint16_t);
}
#endif

#ifndef __INT32_T_SERIALIZERS__
#define __INT32_T_SERIALIZERS__
static inline int serialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(int32_t));
  return sizeof(int32_t);
}

static inline int deserialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(int32_t));
  return sizeof(int32_t);
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_TopicInfo_t(TopicInfo_t* topicinfo)
{
  int bytesize = 0;
  bytesize+=sizeof(uint16_t);
  bytesize+=sizeof(int32_t);
  topicinfo->topic_name_length = strlen(topicinfo->topic_name);
  bytesize+=sizeof(char)*topicinfo->topic_name_length;
  bytesize+=sizeof(int32_t);
  topicinfo->message_type_length = strlen(topicinfo->message_type);
  bytesize+=sizeof(char)*topicinfo->message_type_length;
  bytesize+=sizeof(int32_t);
  topicinfo->md5sum_length = strlen(topicinfo->md5sum);
  bytesize+=sizeof(char)*topicinfo->md5sum_length;
  bytesize+=sizeof(int32_t);
  return bytesize;
}

static int serialize_TopicInfo_t(TopicInfo_t* topicinfo, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_uint16_t(&topicinfo->topic_id, buffer, pos);
  topicinfo->topic_name_length = strlen(topicinfo->topic_name);
  pos+=serialize_int32_t(&topicinfo->topic_name_length, buffer, pos);
  pos+=serialize_char_array(&topicinfo->topic_name[0], buffer, pos, topicinfo->topic_name_length);
  topicinfo->message_type_length = strlen(topicinfo->message_type);
  pos+=serialize_int32_t(&topicinfo->message_type_length, buffer, pos);
  pos+=serialize_char_array(&topicinfo->message_type[0], buffer, pos, topicinfo->message_type_length);
  topicinfo->md5sum_length = strlen(topicinfo->md5sum);
  pos+=serialize_int32_t(&topicinfo->md5sum_length, buffer, pos);
  pos+=serialize_char_array(&topicinfo->md5sum[0], buffer, pos, topicinfo->md5sum_length);
  pos+=serialize_int32_t(&topicinfo->buffer_size, buffer, pos);
  return (pos - start);
}

static int deserialize_TopicInfo_t(TopicInfo_t* topicinfo, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_uint16_t(&topicinfo->topic_id, buffer, pos);
  pos+=deserialize_int32_t(&topicinfo->topic_name_length, buffer, pos);
  topicinfo->topic_name = (char*)malloc(topicinfo->topic_name_length);
  pos+=deserialize_char_array(&topicinfo->topic_name[0], buffer, pos, topicinfo->topic_name_length);
  pos+=deserialize_int32_t(&topicinfo->message_type_length, buffer, pos);
  topicinfo->message_type = (char*)malloc(topicinfo->message_type_length);
  pos+=deserialize_char_array(&topicinfo->message_type[0], buffer, pos, topicinfo->message_type_length);
  pos+=deserialize_int32_t(&topicinfo->md5sum_length, buffer, pos);
  topicinfo->md5sum = (char*)malloc(topicinfo->md5sum_length);
  pos+=deserialize_char_array(&topicinfo->md5sum[0], buffer, pos, topicinfo->md5sum_length);
  pos+=deserialize_int32_t(&topicinfo->buffer_size, buffer, pos);
  return (pos - start);
}

static void delete_TopicInfo_t(TopicInfo_t* topicinfo)
{
  free(topicinfo->topic_name);
  free(topicinfo->message_type);
  free(topicinfo->md5sum);
}

static char* TopicInfo_t_md5sum = "9f195f881246fdfa2798d1d3eebca84a";

static TopicInfo_t rosCreateTopicInfo_t(rosNodeHandle_t *nodehandle)
{
  TopicInfo_t topicinfo;
  topicinfo.mh.nodehandle = nodehandle;
  topicinfo.mh.type = "TopicInfo";
  topicinfo.mh.md5sum = TopicInfo_t_md5sum;
  topicinfo.mh.sizer = bytesize_of_TopicInfo_t;
  topicinfo.mh.serializer = serialize_TopicInfo_t;
  topicinfo.mh.deserializer = deserialize_TopicInfo_t;
  return topicinfo;
}

#endif
