/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __TWISTWITHCOVARIANCE_H__
#define __TWISTWITHCOVARIANCE_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "TwistC.h"

typedef struct
{
  rosMessageHandle_t mh;
  Twist_t twist;
  double covariance[36];
} TwistWithCovariance_t;

#ifndef __DOUBLE_ARRAY_SERIALIZERS__
#define __DOUBLE_ARRAY_SERIALIZERS__
static inline int serialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double)*length);
  return sizeof(double)*length;
}

static inline int deserialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double)*length);
  return sizeof(double)*length;
}
#endif

static int bytesize_of_TwistWithCovariance_t(TwistWithCovariance_t* twistwithcovariance)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Twist_t(&twistwithcovariance->twist);
  bytesize+=sizeof(double)*36;
  return bytesize;
}

static int serialize_TwistWithCovariance_t(TwistWithCovariance_t* twistwithcovariance, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Twist_t(&twistwithcovariance->twist, buffer, pos);
  pos+=serialize_double_array(&twistwithcovariance->covariance[0], buffer, pos, 36);
  return (pos - start);
}

static int deserialize_TwistWithCovariance_t(TwistWithCovariance_t* twistwithcovariance, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Twist_t(&twistwithcovariance->twist, buffer, pos);
  pos+=deserialize_double_array(&twistwithcovariance->covariance[0], buffer, pos, 36);
  return (pos - start);
}

static void delete_TwistWithCovariance_t(TwistWithCovariance_t* twistwithcovariance)
{
  delete_Twist_t(&twistwithcovariance->twist);
}

static char* TwistWithCovariance_t_md5sum = "1fe8a28e6890a4cc3ae4c3ca5c7d82e6";

static TwistWithCovariance_t rosCreateTwistWithCovariance_t(rosNodeHandle_t *nodehandle)
{
  TwistWithCovariance_t twistwithcovariance;
  twistwithcovariance.mh.nodehandle = nodehandle;
  twistwithcovariance.mh.type = "geometry_msgs/TwistWithCovariance";
  twistwithcovariance.mh.md5sum = TwistWithCovariance_t_md5sum;
  twistwithcovariance.mh.sizer = bytesize_of_TwistWithCovariance_t;
  twistwithcovariance.mh.serializer = serialize_TwistWithCovariance_t;
  twistwithcovariance.mh.deserializer = deserialize_TwistWithCovariance_t;
  return twistwithcovariance;
}

#endif