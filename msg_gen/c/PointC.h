/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __POINT_H__
#define __POINT_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  double x;
  double y;
  double z;
} Point_t;

#ifndef __DOUBLE_SERIALIZERS__
#define __DOUBLE_SERIALIZERS__
static inline int serialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double));
  return sizeof(double);
}

static inline int deserialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double));
  return sizeof(double);
}
#endif

static int bytesize_of_Point_t(Point_t* point)
{
  int bytesize = 0;
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  return bytesize;
}

static int serialize_Point_t(Point_t* point, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_double(&point->x, buffer, pos);
  pos+=serialize_double(&point->y, buffer, pos);
  pos+=serialize_double(&point->z, buffer, pos);
  return (pos - start);
}

static int deserialize_Point_t(Point_t* point, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_double(&point->x, buffer, pos);
  pos+=deserialize_double(&point->y, buffer, pos);
  pos+=deserialize_double(&point->z, buffer, pos);
  return (pos - start);
}

static void delete_Point_t(Point_t* point)
{
}

static char* Point_t_md5sum = "4a842b65f413084dc2b10fb484ea7f17";

static Point_t rosCreatePoint_t(rosNodeHandle_t *nodehandle)
{
  Point_t point;
  point.mh.nodehandle = nodehandle;
  point.mh.type = "geometry_msgs/Point";
  point.mh.md5sum = Point_t_md5sum;
  point.mh.sizer = bytesize_of_Point_t;
  point.mh.serializer = serialize_Point_t;
  point.mh.deserializer = deserialize_Point_t;
  return point;
}

#endif
