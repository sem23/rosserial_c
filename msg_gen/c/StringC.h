/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __STRING_H__
#define __STRING_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  char* data;
  int32_t data_length;
} String_t;

#ifndef __INT32_T_SERIALIZERS__
#define __INT32_T_SERIALIZERS__
static inline int serialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(int32_t));
  return sizeof(int32_t);
}

static inline int deserialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(int32_t));
  return sizeof(int32_t);
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_String_t(String_t* string)
{
  int bytesize = 0;
  bytesize+=sizeof(int32_t);
  string->data_length = strlen(string->data);
  bytesize+=sizeof(char)*string->data_length;
  return bytesize;
}

static int serialize_String_t(String_t* string, uint8_t* buffer, int pos)
{
  int start = pos;
  string->data_length = strlen(string->data);
  pos+=serialize_int32_t(&string->data_length, buffer, pos);
  pos+=serialize_char_array(&string->data[0], buffer, pos, string->data_length);
  return (pos - start);
}

static int deserialize_String_t(String_t* string, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_int32_t(&string->data_length, buffer, pos);
  string->data = (char*)malloc(string->data_length);
  pos+=deserialize_char_array(&string->data[0], buffer, pos, string->data_length);
  return (pos - start);
}

static void delete_String_t(String_t* string)
{
  free(string->data);
}

static char* String_t_md5sum = "992ce8a1687cec8c8bd883ec73ca41d1";

static String_t rosCreateString_t(rosNodeHandle_t *nodehandle)
{
  String_t string;
  string.mh.nodehandle = nodehandle;
  string.mh.type = "std_msgs/String";
  string.mh.md5sum = String_t_md5sum;
  string.mh.sizer = bytesize_of_String_t;
  string.mh.serializer = serialize_String_t;
  string.mh.deserializer = deserialize_String_t;
  return string;
}

#endif
