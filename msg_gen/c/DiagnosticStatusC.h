/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __DIAGNOSTICSTATUS_H__
#define __DIAGNOSTICSTATUS_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "KeyValueC.h"

typedef struct
{
  rosMessageHandle_t mh;
  uint8_t level;
  char* name;
  int32_t name_length;
  char* message;
  int32_t message_length;
  char* hardware_id;
  int32_t hardware_id_length;
  KeyValue_t* values;
  int32_t values_length;
} DiagnosticStatus_t;

#ifndef __UINT8_T_SERIALIZERS__
#define __UINT8_T_SERIALIZERS__
static inline int serialize_uint8_t(uint8_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(uint8_t));
  return sizeof(uint8_t);
}

static inline int deserialize_uint8_t(uint8_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(uint8_t));
  return sizeof(uint8_t);
}
#endif

#ifndef __INT32_T_SERIALIZERS__
#define __INT32_T_SERIALIZERS__
static inline int serialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(int32_t));
  return sizeof(int32_t);
}

static inline int deserialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(int32_t));
  return sizeof(int32_t);
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_DiagnosticStatus_t(DiagnosticStatus_t* diagnosticstatus)
{
  int bytesize = 0;
  bytesize+=sizeof(uint8_t);
  bytesize+=sizeof(int32_t);
  diagnosticstatus->name_length = strlen(diagnosticstatus->name);
  bytesize+=sizeof(char)*diagnosticstatus->name_length;
  bytesize+=sizeof(int32_t);
  diagnosticstatus->message_length = strlen(diagnosticstatus->message);
  bytesize+=sizeof(char)*diagnosticstatus->message_length;
  bytesize+=sizeof(int32_t);
  diagnosticstatus->hardware_id_length = strlen(diagnosticstatus->hardware_id);
  bytesize+=sizeof(char)*diagnosticstatus->hardware_id_length;
  bytesize+=sizeof(int32_t);
  int i;
  for(i = 0; i < diagnosticstatus->values_length; ++i)
    bytesize+=bytesize_of_KeyValue_t(&diagnosticstatus->values[i]);
  return bytesize;
}

static int serialize_DiagnosticStatus_t(DiagnosticStatus_t* diagnosticstatus, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_uint8_t(&diagnosticstatus->level, buffer, pos);
  diagnosticstatus->name_length = strlen(diagnosticstatus->name);
  pos+=serialize_int32_t(&diagnosticstatus->name_length, buffer, pos);
  pos+=serialize_char_array(&diagnosticstatus->name[0], buffer, pos, diagnosticstatus->name_length);
  diagnosticstatus->message_length = strlen(diagnosticstatus->message);
  pos+=serialize_int32_t(&diagnosticstatus->message_length, buffer, pos);
  pos+=serialize_char_array(&diagnosticstatus->message[0], buffer, pos, diagnosticstatus->message_length);
  diagnosticstatus->hardware_id_length = strlen(diagnosticstatus->hardware_id);
  pos+=serialize_int32_t(&diagnosticstatus->hardware_id_length, buffer, pos);
  pos+=serialize_char_array(&diagnosticstatus->hardware_id[0], buffer, pos, diagnosticstatus->hardware_id_length);
  pos+=serialize_int32_t(&diagnosticstatus->values_length, buffer, pos);
  int i;
  for(i = 0; i < diagnosticstatus->values_length; ++i)
    pos+=serialize_KeyValue_t(&diagnosticstatus->values[i], buffer, pos);
  return (pos - start);
}

static int deserialize_DiagnosticStatus_t(DiagnosticStatus_t* diagnosticstatus, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_uint8_t(&diagnosticstatus->level, buffer, pos);
  pos+=deserialize_int32_t(&diagnosticstatus->name_length, buffer, pos);
  diagnosticstatus->name = (char*)malloc(diagnosticstatus->name_length);
  pos+=deserialize_char_array(&diagnosticstatus->name[0], buffer, pos, diagnosticstatus->name_length);
  pos+=deserialize_int32_t(&diagnosticstatus->message_length, buffer, pos);
  diagnosticstatus->message = (char*)malloc(diagnosticstatus->message_length);
  pos+=deserialize_char_array(&diagnosticstatus->message[0], buffer, pos, diagnosticstatus->message_length);
  pos+=deserialize_int32_t(&diagnosticstatus->hardware_id_length, buffer, pos);
  diagnosticstatus->hardware_id = (char*)malloc(diagnosticstatus->hardware_id_length);
  pos+=deserialize_char_array(&diagnosticstatus->hardware_id[0], buffer, pos, diagnosticstatus->hardware_id_length);
  pos+=deserialize_int32_t(&diagnosticstatus->values_length, buffer, pos);
  int i;
  for(i = 0; i < diagnosticstatus->values_length; ++i)
    pos+=deserialize_KeyValue_t(&diagnosticstatus->values[i], buffer, pos);
  return (pos - start);
}

static void delete_DiagnosticStatus_t(DiagnosticStatus_t* diagnosticstatus)
{
  free(diagnosticstatus->name);
  free(diagnosticstatus->message);
  free(diagnosticstatus->hardware_id);
}

static char* DiagnosticStatus_t_md5sum = "67d15a62edb26e9d52b0f0efa3ef9da7";

static DiagnosticStatus_t rosCreateDiagnosticStatus_t(rosNodeHandle_t *nodehandle)
{
  DiagnosticStatus_t diagnosticstatus;
  diagnosticstatus.mh.nodehandle = nodehandle;
  diagnosticstatus.mh.type = "diagnostic_msgs/DiagnosticStatus";
  diagnosticstatus.mh.md5sum = DiagnosticStatus_t_md5sum;
  diagnosticstatus.mh.sizer = bytesize_of_DiagnosticStatus_t;
  diagnosticstatus.mh.serializer = serialize_DiagnosticStatus_t;
  diagnosticstatus.mh.deserializer = deserialize_DiagnosticStatus_t;
  return diagnosticstatus;
}

#endif
