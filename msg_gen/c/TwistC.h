/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __TWIST_H__
#define __TWIST_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "Vector3C.h"

typedef struct
{
  rosMessageHandle_t mh;
  Vector3_t linear;
  Vector3_t angular;
} Twist_t;

static int bytesize_of_Twist_t(Twist_t* twist)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Vector3_t(&twist->linear);
  bytesize+=bytesize_of_Vector3_t(&twist->angular);
  return bytesize;
}

static int serialize_Twist_t(Twist_t* twist, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Vector3_t(&twist->linear, buffer, pos);
  pos+=serialize_Vector3_t(&twist->angular, buffer, pos);
  return (pos - start);
}

static int deserialize_Twist_t(Twist_t* twist, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Vector3_t(&twist->linear, buffer, pos);
  pos+=deserialize_Vector3_t(&twist->angular, buffer, pos);
  return (pos - start);
}

static void delete_Twist_t(Twist_t* twist)
{
  delete_Vector3_t(&twist->linear);
  delete_Vector3_t(&twist->angular);
}

static char* Twist_t_md5sum = "9f195f881246fdfa2798d1d3eebca84a";

static Twist_t rosCreateTwist_t(rosNodeHandle_t *nodehandle)
{
  Twist_t twist;
  twist.mh.nodehandle = nodehandle;
  twist.mh.type = "geometry_msgs/Twist";
  twist.mh.md5sum = Twist_t_md5sum;
  twist.mh.sizer = bytesize_of_Twist_t;
  twist.mh.serializer = serialize_Twist_t;
  twist.mh.deserializer = deserialize_Twist_t;
  return twist;
}

#endif
