/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __IMU_H__
#define __IMU_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "HeaderC.h"
#include "QuaternionC.h"
#include "Vector3C.h"

typedef struct
{
  rosMessageHandle_t mh;
  Header_t header;
  Quaternion_t orientation;
  double orientation_covariance[9];
  Vector3_t angular_velocity;
  double angular_velocity_covariance[9];
  Vector3_t linear_acceleration;
  double linear_acceleration_covariance[9];
} Imu_t;

#ifndef __DOUBLE_ARRAY_SERIALIZERS__
#define __DOUBLE_ARRAY_SERIALIZERS__
static inline int serialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double)*length);
  return sizeof(double)*length;
}

static inline int deserialize_double_array(double* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double)*length);
  return sizeof(double)*length;
}
#endif

static int bytesize_of_Imu_t(Imu_t* imu)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Header_t(&imu->header);
  bytesize+=bytesize_of_Quaternion_t(&imu->orientation);
  bytesize+=sizeof(double)*9;
  bytesize+=bytesize_of_Vector3_t(&imu->angular_velocity);
  bytesize+=sizeof(double)*9;
  bytesize+=bytesize_of_Vector3_t(&imu->linear_acceleration);
  bytesize+=sizeof(double)*9;
  return bytesize;
}

static int serialize_Imu_t(Imu_t* imu, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Header_t(&imu->header, buffer, pos);
  pos+=serialize_Quaternion_t(&imu->orientation, buffer, pos);
  pos+=serialize_double_array(&imu->orientation_covariance[0], buffer, pos, 9);
  pos+=serialize_Vector3_t(&imu->angular_velocity, buffer, pos);
  pos+=serialize_double_array(&imu->angular_velocity_covariance[0], buffer, pos, 9);
  pos+=serialize_Vector3_t(&imu->linear_acceleration, buffer, pos);
  pos+=serialize_double_array(&imu->linear_acceleration_covariance[0], buffer, pos, 9);
  return (pos - start);
}

static int deserialize_Imu_t(Imu_t* imu, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Header_t(&imu->header, buffer, pos);
  pos+=deserialize_Quaternion_t(&imu->orientation, buffer, pos);
  pos+=deserialize_double_array(&imu->orientation_covariance[0], buffer, pos, 9);
  pos+=deserialize_Vector3_t(&imu->angular_velocity, buffer, pos);
  pos+=deserialize_double_array(&imu->angular_velocity_covariance[0], buffer, pos, 9);
  pos+=deserialize_Vector3_t(&imu->linear_acceleration, buffer, pos);
  pos+=deserialize_double_array(&imu->linear_acceleration_covariance[0], buffer, pos, 9);
  return (pos - start);
}

static void delete_Imu_t(Imu_t* imu)
{
  delete_Header_t(&imu->header);
  delete_Quaternion_t(&imu->orientation);
  delete_Vector3_t(&imu->angular_velocity);
  delete_Vector3_t(&imu->linear_acceleration);
}

static char* Imu_t_md5sum = "6a62c6daae103f4ff57a132d6f95cec2";

static Imu_t rosCreateImu_t(rosNodeHandle_t *nodehandle)
{
  Imu_t imu;
  imu.mh.nodehandle = nodehandle;
  imu.mh.type = "sensor_msgs/Imu";
  imu.mh.md5sum = Imu_t_md5sum;
  imu.mh.sizer = bytesize_of_Imu_t;
  imu.mh.serializer = serialize_Imu_t;
  imu.mh.deserializer = deserialize_Imu_t;
  return imu;
}

#endif
