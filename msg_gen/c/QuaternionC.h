/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  double x;
  double y;
  double z;
  double w;
} Quaternion_t;

#ifndef __DOUBLE_SERIALIZERS__
#define __DOUBLE_SERIALIZERS__
static inline int serialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double));
  return sizeof(double);
}

static inline int deserialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double));
  return sizeof(double);
}
#endif

static int bytesize_of_Quaternion_t(Quaternion_t* quaternion)
{
  int bytesize = 0;
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  return bytesize;
}

static int serialize_Quaternion_t(Quaternion_t* quaternion, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_double(&quaternion->x, buffer, pos);
  pos+=serialize_double(&quaternion->y, buffer, pos);
  pos+=serialize_double(&quaternion->z, buffer, pos);
  pos+=serialize_double(&quaternion->w, buffer, pos);
  return (pos - start);
}

static int deserialize_Quaternion_t(Quaternion_t* quaternion, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_double(&quaternion->x, buffer, pos);
  pos+=deserialize_double(&quaternion->y, buffer, pos);
  pos+=deserialize_double(&quaternion->z, buffer, pos);
  pos+=deserialize_double(&quaternion->w, buffer, pos);
  return (pos - start);
}

static void delete_Quaternion_t(Quaternion_t* quaternion)
{
}

static char* Quaternion_t_md5sum = "a779879fadf0160734f906b8c19c7004";

static Quaternion_t rosCreateQuaternion_t(rosNodeHandle_t *nodehandle)
{
  Quaternion_t quaternion;
  quaternion.mh.nodehandle = nodehandle;
  quaternion.mh.type = "Quaternion";
  quaternion.mh.md5sum = Quaternion_t_md5sum;
  quaternion.mh.sizer = bytesize_of_Quaternion_t;
  quaternion.mh.serializer = serialize_Quaternion_t;
  quaternion.mh.deserializer = deserialize_Quaternion_t;
  return quaternion;
}

#endif
