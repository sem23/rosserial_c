/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __DIAGNOSTICARRAY_H__
#define __DIAGNOSTICARRAY_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"

#include "HeaderC.h"
#include "DiagnosticStatusC.h"

typedef struct
{
  rosMessageHandle_t mh;
  Header_t header;
  DiagnosticStatus_t* status;
  int32_t status_length;
} DiagnosticArray_t;

static int bytesize_of_DiagnosticArray_t(DiagnosticArray_t* diagnosticarray)
{
  int bytesize = 0;
  bytesize+=bytesize_of_Header_t(&diagnosticarray->header);
  bytesize+=sizeof(int32_t);
  int i;
  for(i = 0; i < diagnosticarray->status_length; ++i)
    bytesize+=bytesize_of_DiagnosticStatus_t(&diagnosticarray->status[i]);
  return bytesize;
}

static int serialize_DiagnosticArray_t(DiagnosticArray_t* diagnosticarray, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_Header_t(&diagnosticarray->header, buffer, pos);
  pos+=serialize_int32_t(&diagnosticarray->status_length, buffer, pos);
  int i;
  for(i = 0; i < diagnosticarray->status_length; ++i)
    pos+=serialize_DiagnosticStatus_t(&diagnosticarray->status[i], buffer, pos);
  return (pos - start);
}

static int deserialize_DiagnosticArray_t(DiagnosticArray_t* diagnosticarray, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_Header_t(&diagnosticarray->header, buffer, pos);
  pos+=deserialize_int32_t(&diagnosticarray->status_length, buffer, pos);
  int i;
  for(i = 0; i < diagnosticarray->status_length; ++i)
    pos+=deserialize_DiagnosticStatus_t(&diagnosticarray->status[i], buffer, pos);
  return (pos - start);
}

static void delete_DiagnosticArray_t(DiagnosticArray_t* diagnosticarray)
{
  delete_Header_t(&diagnosticarray->header);
}

static char* DiagnosticArray_t_md5sum = "3cfbeff055e708a24c3d946a5c8139cd";

static DiagnosticArray_t rosCreateDiagnosticArray_t(rosNodeHandle_t *nodehandle)
{
  DiagnosticArray_t diagnosticarray;
  diagnosticarray.mh.nodehandle = nodehandle;
  diagnosticarray.mh.type = "diagnostic_msgs/DiagnosticArray";
  diagnosticarray.mh.md5sum = DiagnosticArray_t_md5sum;
  diagnosticarray.mh.sizer = bytesize_of_DiagnosticArray_t;
  diagnosticarray.mh.serializer = serialize_DiagnosticArray_t;
  diagnosticarray.mh.deserializer = deserialize_DiagnosticArray_t;
  return diagnosticarray;
}

#endif
