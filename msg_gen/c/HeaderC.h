/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __HEADER_H__
#define __HEADER_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  uint32_t seq;
  union
  {
    rosTime_t stamp;
    uint32_t stamp_[2];
  };
  char* frame_id;
  int32_t frame_id_length;
} Header_t;

#ifndef __UINT32_T_SERIALIZERS__
#define __UINT32_T_SERIALIZERS__
static inline int serialize_uint32_t(uint32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(uint32_t));
  return sizeof(uint32_t);
}

static inline int deserialize_uint32_t(uint32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(uint32_t));
  return sizeof(uint32_t);
}
#endif

#ifndef __UINT32_T_ARRAY_SERIALIZERS__
#define __UINT32_T_ARRAY_SERIALIZERS__
static inline int serialize_uint32_t_array(uint32_t* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(uint32_t)*length);
  return sizeof(uint32_t)*length;
}

static inline int deserialize_uint32_t_array(uint32_t* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(uint32_t)*length);
  return sizeof(uint32_t)*length;
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_Header_t(Header_t* header)
{
  int bytesize = 0;
  bytesize+=sizeof(uint32_t);
  bytesize+=sizeof(uint32_t)*2;
  bytesize+=sizeof(int32_t);
  header->frame_id_length = strlen(header->frame_id);
  bytesize+=sizeof(char)*header->frame_id_length;
  return bytesize;
}

static int serialize_Header_t(Header_t* header, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_uint32_t(&header->seq, buffer, pos);
  pos+=serialize_uint32_t_array(&header->stamp_[0], buffer, pos, 2);
  header->frame_id_length = strlen(header->frame_id);
  pos+=serialize_int32_t(&header->frame_id_length, buffer, pos);
  pos+=serialize_char_array(&header->frame_id[0], buffer, pos, header->frame_id_length);
  return (pos - start);
}

static int deserialize_Header_t(Header_t* header, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_uint32_t(&header->seq, buffer, pos);
  pos+=deserialize_uint32_t_array(&header->stamp_[0], buffer, pos, 2);
  pos+=deserialize_int32_t(&header->frame_id_length, buffer, pos);
  header->frame_id = (char*)malloc(header->frame_id_length);
  pos+=deserialize_char_array(&header->frame_id[0], buffer, pos, header->frame_id_length);
  return (pos - start);
}

static void delete_Header_t(Header_t* header)
{
  free(header->frame_id);
}

static char* Header_t_md5sum = "2176decaecbce78abc3b96ef049fabed";

static Header_t rosCreateHeader_t(rosNodeHandle_t *nodehandle)
{
  Header_t header;
  header.mh.nodehandle = nodehandle;
  header.mh.type = "std_msgs/Header";
  header.mh.md5sum = Header_t_md5sum;
  header.mh.sizer = bytesize_of_Header_t;
  header.mh.serializer = serialize_Header_t;
  header.mh.deserializer = deserialize_Header_t;
  return header;
}

#endif
