/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __KEYVALUE_H__
#define __KEYVALUE_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  char* key;
  int32_t key_length;
  char* value;
  int32_t value_length;
} KeyValue_t;

#ifndef __INT32_T_SERIALIZERS__
#define __INT32_T_SERIALIZERS__
static inline int serialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(int32_t));
  return sizeof(int32_t);
}

static inline int deserialize_int32_t(int32_t* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(int32_t));
  return sizeof(int32_t);
}
#endif

#ifndef __CHAR_ARRAY_SERIALIZERS__
#define __CHAR_ARRAY_SERIALIZERS__
static inline int serialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(char)*length);
  return sizeof(char)*length;
}

static inline int deserialize_char_array(char* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(char)*length);
  return sizeof(char)*length;
}
#endif

static int bytesize_of_KeyValue_t(KeyValue_t* keyvalue)
{
  int bytesize = 0;
  bytesize+=sizeof(int32_t);
  keyvalue->key_length = strlen(keyvalue->key);
  bytesize+=sizeof(char)*keyvalue->key_length;
  bytesize+=sizeof(int32_t);
  keyvalue->value_length = strlen(keyvalue->value);
  bytesize+=sizeof(char)*keyvalue->value_length;
  return bytesize;
}

static int serialize_KeyValue_t(KeyValue_t* keyvalue, uint8_t* buffer, int pos)
{
  int start = pos;
  keyvalue->key_length = strlen(keyvalue->key);
  pos+=serialize_int32_t(&keyvalue->key_length, buffer, pos);
  pos+=serialize_char_array(&keyvalue->key[0], buffer, pos, keyvalue->key_length);
  keyvalue->value_length = strlen(keyvalue->value);
  pos+=serialize_int32_t(&keyvalue->value_length, buffer, pos);
  pos+=serialize_char_array(&keyvalue->value[0], buffer, pos, keyvalue->value_length);
  return (pos - start);
}

static int deserialize_KeyValue_t(KeyValue_t* keyvalue, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_int32_t(&keyvalue->key_length, buffer, pos);
  keyvalue->key = (char*)malloc(keyvalue->key_length);
  pos+=deserialize_char_array(&keyvalue->key[0], buffer, pos, keyvalue->key_length);
  pos+=deserialize_int32_t(&keyvalue->value_length, buffer, pos);
  keyvalue->value = (char*)malloc(keyvalue->value_length);
  pos+=deserialize_char_array(&keyvalue->value[0], buffer, pos, keyvalue->value_length);
  return (pos - start);
}

static void delete_KeyValue_t(KeyValue_t* keyvalue)
{
  free(keyvalue->key);
  free(keyvalue->value);
}

static char* KeyValue_t_md5sum = "cf57fdc6617a881a88c16e768132149c";

static KeyValue_t rosCreateKeyValue_t(rosNodeHandle_t *nodehandle)
{
  KeyValue_t keyvalue;
  keyvalue.mh.nodehandle = nodehandle;
  keyvalue.mh.type = "diagnostic_msgs/KeyValue";
  keyvalue.mh.md5sum = KeyValue_t_md5sum;
  keyvalue.mh.sizer = bytesize_of_KeyValue_t;
  keyvalue.mh.serializer = serialize_KeyValue_t;
  keyvalue.mh.deserializer = deserialize_KeyValue_t;
  return keyvalue;
}

#endif
