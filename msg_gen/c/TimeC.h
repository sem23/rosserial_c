/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __TIME_H__
#define __TIME_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  union
  {
    rosTime_t data;
    uint32_t data_[2];
  };
} Time_t;

#ifndef __UINT32_T_ARRAY_SERIALIZERS__
#define __UINT32_T_ARRAY_SERIALIZERS__
static inline int serialize_uint32_t_array(uint32_t* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(uint32_t)*length);
  return sizeof(uint32_t)*length;
}

static inline int deserialize_uint32_t_array(uint32_t* data, uint8_t* buffer, int pos, int length)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(uint32_t)*length);
  return sizeof(uint32_t)*length;
}
#endif

static int bytesize_of_Time_t(Time_t* time)
{
  int bytesize = 0;
  bytesize+=sizeof(uint32_t)*2;
  return bytesize;
}

static int serialize_Time_t(Time_t* time, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_uint32_t_array(&time->data_[0], buffer, pos, 2);
  return (pos - start);
}

static int deserialize_Time_t(Time_t* time, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_uint32_t_array(&time->data_[0], buffer, pos, 2);
  return (pos - start);
}

static void delete_Time_t(Time_t* time)
{
}

static char* Time_t_md5sum = "cd7166c74c552c311fbcc2fe5a7bc289";

static Time_t rosCreateTime_t(rosNodeHandle_t *nodehandle)
{
  Time_t time;
  time.mh.nodehandle = nodehandle;
  time.mh.type = "std_msgs/Time";
  time.mh.md5sum = Time_t_md5sum;
  time.mh.sizer = bytesize_of_Time_t;
  time.mh.serializer = serialize_Time_t;
  time.mh.deserializer = deserialize_Time_t;
  return time;
}

#endif
