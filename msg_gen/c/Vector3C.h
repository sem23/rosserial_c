/*********************************************************
 *             ROS C message generator!                  *
 *********************************************************
 * This piece of code was written to help industrial     *
 * manufacturers building up some ROS-enabled sensors!   *
 * (i.e. ROS-compatible sensor firmware + target nodes)  *
 * Generated code is C89 conform and should be usable as *
 * starting point for any MCU having a U(S)ART and       *
 * understanding C89!                                    *
 *********************************************************
 * Version: 0.2.3                                        *
 * Authors: Peter Rudolph                                *
 *********************************************************/

#ifndef __VECTOR3_H__
#define __VECTOR3_H__

#include <stdint.h>
#include <stdlib.h> /* malloc & free */
#include <string.h> /* memcpy */
#include "ros.h"


typedef struct
{
  rosMessageHandle_t mh;
  double x;
  double y;
  double z;
} Vector3_t;

#ifndef __DOUBLE_SERIALIZERS__
#define __DOUBLE_SERIALIZERS__
static inline int serialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) &buffer[pos], (void *) data, sizeof(double));
  return sizeof(double);
}

static inline int deserialize_double(double* data, uint8_t* buffer, int pos)
{
  memcpy((void *) data, (void *) &buffer[pos], sizeof(double));
  return sizeof(double);
}
#endif

static int bytesize_of_Vector3_t(Vector3_t* vector3)
{
  int bytesize = 0;
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  bytesize+=sizeof(double);
  return bytesize;
}

static int serialize_Vector3_t(Vector3_t* vector3, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=serialize_double(&vector3->x, buffer, pos);
  pos+=serialize_double(&vector3->y, buffer, pos);
  pos+=serialize_double(&vector3->z, buffer, pos);
  return (pos - start);
}

static int deserialize_Vector3_t(Vector3_t* vector3, uint8_t* buffer, int pos)
{
  int start = pos;
  pos+=deserialize_double(&vector3->x, buffer, pos);
  pos+=deserialize_double(&vector3->y, buffer, pos);
  pos+=deserialize_double(&vector3->z, buffer, pos);
  return (pos - start);
}

static void delete_Vector3_t(Vector3_t* vector3)
{
}

static char* Vector3_t_md5sum = "4a842b65f413084dc2b10fb484ea7f17";

static Vector3_t rosCreateVector3_t(rosNodeHandle_t *nodehandle)
{
  Vector3_t vector3;
  vector3.mh.nodehandle = nodehandle;
  vector3.mh.type = "Vector3";
  vector3.mh.md5sum = Vector3_t_md5sum;
  vector3.mh.sizer = bytesize_of_Vector3_t;
  vector3.mh.serializer = serialize_Vector3_t;
  vector3.mh.deserializer = deserialize_Vector3_t;
  return vector3;
}

#endif
