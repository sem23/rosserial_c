/**
 *  @file ros_os.h
 *  @brief ROS Serial C OS abstraction layer
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_OS_H__
#define __ROS_OS_H__
/* usage hint */
#ifndef ROS_USE_OS
#error "If you want to use an OS you have to define ROS_USE_OS first."
#endif
/* ROS includes */
#include "ros_types.h"
/**
 *  @brief Initialize OS.
 *
 *    If your OS needs to initialize something special you can do it here.
 */
rosReturnCode_t osInit();
/**
 *  @brief Lock OS critical.
 *
 *    While message operations on interfaces are done OS needs to be locked.
 */
rosReturnCode_t osLockCritical();
/**
 *  @brief Unlock OS critical.
 *
 *    Unlock previously locked OS.
 */
rosReturnCode_t osUnlockCritical();
/**
 *  @brief Sleep milliseconds.
 *
 *    Sleep given time in milliseconds.
 *
 *  @param ms Milliseconds to sleep.
 */
void osMSleep(uint32_t ms);
/**
 *  @brief Sleep microseconds.
 *
 *    Sleep given time in microseconds.
 *
 *  @param us Microseconds to sleep.
 */
void osUSleep(uint32_t us);
/**
 *  @brief Delay OS until given timestamp.
 *
 *    Delay OS from last wake time until achieving given time increment.
 *    To work as thought last_wake_time has to be updated every time, the
 *    function is called. While using OS, the implemention is needed by
 *    rosSleepRate(rosRate* rate).
 *
 *  @param last_wake_time Last time thread was woken up.
 *  @param time_increment Delay till next wake up.
 *
 */
void osDelayUntil(uint32_t* last_wake_time, uint32_t time_increment);
/**
 *  @brief Start ROS Node in OS way.
 *
 *    Wrapper for starting a ROS Node as a OS task. This function can be
 *    called automatically by defining ROS_START_NODES_WITH_OS.
 *
 */
rosReturnCode_t osStartNode(osROSNode_t* node);

#endif
