/**
 *  @file ros_OS_TEMPLATE.c
 *  @brief ROS Serial C client library OS layer implementation TEMPLATE
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
/* ROS includes */
#include "ros.h"
/* OS include */
/* TODO add OS dependeing includes here */

rosReturnCode_t osInit()
{
  /* Initialize OS */
  /* TODO Initialize OS specifics, you can start the scheduler here!
     Function is called at the end of rosInit() and if you start the nodes
     before scheduler, everything is fine if it never returns. */
  /* return ROS_SUCCESS on success or ROS_FAIL on failure */
  /* TODO return code */
}

rosReturnCode_t osLockCritical()
{
  /* enter critical section */
  /* TODO lock system here here*/
  /* return ROS_SUCCESS on success or ROS_FAIL on failure */
  /* TODO return code */
}

rosReturnCode_t osUnlockCritical()
{
  /* leave critical section */
  /* TODO unlock system here here*/
  /* return ROS_SUCCESS on success or ROS_FAIL on failure */
  /* TODO return code */
}

void osMSleep(uint32_t ms)
{
  /* sleep given time in milliseconds */
  /* TODO task should sleep given timespan in milliseconds */
}

void osUSleep(uint32_t us)
{
  /* sleep given time in microseconds */
  /* TODO task should sleep given timespan in microseconds */
}

void osDelayUntil(uint32_t* last_wake_time, uint32_t time_increment)
{
  /* Sleep until */
  /* TODO task should sleep rest of timespan between last_wake_time and
     last_wake_time+time_increment. Also last_wake_time should be updated
     to the resulting wake time. */
}

rosReturnCode_t osStartNode(osROSNode_t* node)
{
  /* Start Node as Task */
  /* TODO call a OS specifice task create function with given arguments. */
}