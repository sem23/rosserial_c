/* ROS OS abstracrion layer implementation for FreeRTOS */

/* ROS */
#include "ros.h"
/* FreeRTOS */
#include "FreeRTOS.h"
#include "task.h"

rosReturnCode_t osInit()
{
  /* Start the scheduler. */
  vTaskStartScheduler();
  /* Will only get here if there was insufficient memory to create the idle
  task.  The idle task is created within vTaskStartScheduler(). */
  return ROS_NOT_OK;
}

rosReturnCode_t osLockCritical()
{
  /* enter critical section */
  portENTER_CRITICAL();
}

rosReturnCode_t osUnlockCritical()
{
  /* leave critical section */
  portEXIT_CRITICAL();
}

void osMSleep(uint32_t millisec)
{
  /* get milliseconds as ticks */
  portTickType msec_delay = millisec / portTICK_RATE_MS;
  /* lay task sleeping */
  vTaskDelay(msec_delay);
}

void osDelayUntil(uint32_t* last_wake_time, uint32_t time_increment)
{
  /* FreeRTOS provides this function itself, so we just call it */
  vTaskDelayUntil(last_wake_time, time_increment);
}

rosReturnCode_t osStartNode(osROSNode_t* node)
{
  /* Start Node as Task */
  xTaskCreate( 
    node->node_func, 
    node->name, 
    node->stack_size, 
    node->args, 
    node->priority, 
    NULL 
  );  
}