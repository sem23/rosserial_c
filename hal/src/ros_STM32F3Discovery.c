/* ROS includes */
#include "ros_hardware.h"
/* hardware configurations based on STM32F30x_StdPeriph_Driver */
#include "hw_config.h"
/* USB Virtual COM Port configurations based on STM32_USB-FS_Device_Driver */
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"

/* STM32 FW Lib RCC */
RCC_ClocksTypeDef RCC_Clocks;

/* global controller lifetime */
extern uint32_t g_lifetime;

rosReturnCode_t hardwareInit()
{
  /* initialize system */
  Set_System();
  /* SysTick end of count event each 1ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
  /* initialize LEDs */
  STM_EVAL_LEDInit(LED4);
  STM_EVAL_LEDInit(LED5);
  STM_EVAL_LEDInit(LED6);
  STM_EVAL_LEDInit(LED7);
  STM_EVAL_LEDInit(LED8);
  STM_EVAL_LEDInit(LED9);
  /* initialize USB VCP */
  Set_USBClock();
  USB_Interrupts_Config();
  USB_Init();
  /* return code */
  return ROS_SUCCESS;
}

void hardwareToggleLED(LED_t led)
{
  /* toggle given LED */
  switch(led)
  {
  case SPIN_LED:
    STM_EVAL_LEDToggle(LED4);
    break;
  case SYNC_LED:
    STM_EVAL_LEDToggle(LED5);
    break;
  case LOCK_LED:
    STM_EVAL_LEDToggle(LED6);
    break;
  case ERROR_LED:
    STM_EVAL_LEDToggle(LED7);
    break;
  default:
    break;
  }
}

uint32_t hardwareGetTime()
{
  /* return global controller lifetime */
  return g_lifetime;
}