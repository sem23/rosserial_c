/* ROS includes */
#include "ros_hardware.h"
/* hardware includes e.g. firmware/driver */

/* global controller lifetime */
extern uint32_t g_lifetime;

rosReturnCode_t hardwareInit()
{
  /* TODO initialize system */
  /* TODO set SysTick event each 1ms */
  /* TODO initialize LEDs */
  /* TODO initialize some other hardware */
  /* return code */
  return ROS_SUCCESS;
}

void hardwareToggleLED(LED_t led)
{
  /* toggle given LED */
  switch(led)
  {
  case SPIN_LED:
    /* TODO toggle an LED if possible */
    break;
  case SYNC_LED:
    /* TODO toggle an LED if possible */
    break;
  case LOCK_LED:
    /* TODO toggle an LED if possible */
    break;
  case ERROR_LED:
    /* TODO toggle an LED if possible */
    break;
  default:
    break;
  }
}

uint32_t hardwareGetTime()
{
  /* return global controller lifetime */
  return g_lifetime;
}