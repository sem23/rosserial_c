/**
 *  @file ros_hardware.h
 *  @brief ROS Serial C hardware abstraction header
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_HARDWARE_H__
#define __ROS_HARDWARE_H__
/* ROS includes */
#include "ros_types.h"
/**
 *  @brief LED enumerator.
 *
 *      Indicates different signals or states.
 */
typedef enum LED
{
  SPIN_LED, /**< rosSpin() or rosSpinOnce() is called */
  SYNC_LED, /**< ROS is syncing */
  LOCK_LED, /**< System is locked for interface operations */
  ERROR_LED, /**< An error occured */
} LED_t;
/**
 *  @brief Initialize current hardware.
 *  @return controller life time in milliseconds
 */
rosReturnCode_t hardwareInit();
/**
 *  @brief Get current hardware time.
 *
 *      Implementation must return uC lifetime in milliseconds. Either use SysTick interrupt
 *      or use OS specific function, e.g. tick-hooks.
 *
 *  @return controller life time in milliseconds
 */
uint32_t hardwareGetTime();
/**
 *  @brief Toggles specific hardware LED.
 *  @param byte pointer to byte where value is send from
 */
void hardwareToggleLED(LED_t led);

#endif /* #ifndef __ROS_HARDWARE_H__ */
