/**
 *  @file ros_types.h
 *  @brief ROS Serial C type definitions
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_TYPES_H__
#define __ROS_TYPES_H__
/* ROS includes */
#include "ros_std.h"
#include "ros_config.h"
/******************************************************/
/*                                                    */
/*                  PROTOTYPES                        */
/*                                                    */
/******************************************************/
typedef struct rosMessageHandle rosMessageHandle_t;
/******************************************************/
/*                                                    */
/*                 RETURN CODES                       */
/*                                                    */
/******************************************************/
/**
 * @brief Enumerator defining ROS return codes
 */
typedef enum rosReturnCode
{
  ROS_NOT_OK = 0, /**< ROS is not ok */
  ROS_OK, /**< ROS is ok */
  ROS_SUCCESS, /**< operation succeeded */
  ROS_FAIL, /**< operation failed */
  ROS_INIT_NOT_CALLED, /**< rosInit was not called */
  ROS_NODEHANDLE_NOT_CONFIGURED, /**< topics are not negotiated, negotiation possibly failed */
  ROS_CONFIGURING, /**< topic negotiation is running */
  ROS_OUT_OF_MEMORY, /**< system is out of memory */
  ROS_FIFO_BUFFER_EMPTY, /**< fifo input buffer is empty */
  ROS_FIFO_BUFFER_OVERFLOW, /**< fifo input buffer is overflowed, you must read bytes from input buffer before you can store new */
} rosReturnCode_t;
/******************************************************/
/*                                                    */
/*                 FIFO BUFFER                        */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold fifo buffer.
 */
typedef struct rosFifoBuffer
{
  uint8_t *data; /**< pointer to fifo buffer array */
  uint8_t read_index; /**< read indexer */
  uint8_t write_index; /**< write indexer */
  rosReturnCode_t (*write)(uint8_t* byte); /**< write funtion */
  rosReturnCode_t (*read)(uint8_t* byte); /**< read funtion */
} rosFifoBuffer_t;
/******************************************************/
/*                                                    */
/*                    PACKET                          */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold serialized messages.
 */
typedef struct rosPacket
{
  union
  {
    uint8_t sync_flag_[2]; /**< array used to store sync flag */
    uint16_t sync_flag; /**< sync flag */
  };
  union
  {
    uint8_t topic_id_[2]; /**< array used to store topic id */
    uint16_t topic_id; /**< topic id */
  };
  union
  {
    uint8_t message_length_[2]; /**< array used to store message length */
    uint16_t message_length; /**< message length */
  };
  uint8_t* buffer; /**< pointer to packets serialized message data */
  uint16_t buffer_size; /**< pointer to packets serialized message data */
  uint8_t checksum; /**< packet checksum */
} rosPacket_t;
/******************************************************/
/*                                                    */
/*                     TIME                           */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold time information.
 */
typedef struct rosTime
{
  uint32_t sec;
  uint32_t nsec;
} rosTime_t;
/**
 * @brief Structure to hold time rate information.
 */
typedef struct rosRate
{
  uint32_t last_wake_time;
  uint32_t frequency;
} rosRate_t;
/******************************************************/
/*                                                    */
/*                  PUBLISHER                         */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold information about publisher and its messagehandle.
 */
typedef struct rosPublisher
{
  rosMessageHandle_t* messagehandle; /**< pointer to messagehandle */
  rosReturnCode_t (*publish)(void* message);  /**< function pointer to publish function */
} rosPublisher_t;
/******************************************************/
/*                                                    */
/*                  SUBSCRIBER                        */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold information about subscriber and its messagehandle.
 */
typedef struct rosSubscriber
{
  rosMessageHandle_t* messagehandle; /**< pointer to messagehandle */
  void (*callback)(void* message);  /**< function pointer to subscribers callback function */
} rosSubscriber_t;
/******************************************************/
/*                                                    */
/*                  INTERFACE                         */
/*                                                    */
/******************************************************/
/**
 *  @brief Enumerator defining interface types.
 */
typedef enum INTERFACE_TYPE
{
  SLAVE,
  MASTER,
  BRIDGE,
  LOOPBACK,
} rosInterfaceType_t;
/**
 *  @brief Structure to hold information about nodehandles interface.
 */
typedef struct rosInterface
{
  char* device_id; /**< Device id used to find nodehandle by human-readable string */
  rosReturnCode_t (*send)(void* data); /**< Pointer to interface send function */
  rosReturnCode_t (*receive)(void* data); /**< Pointer to interface receive function */
//  bool package_oriented; /**< Used to determine if send function is called once or more */
  rosInterfaceType_t interface_type; /**< Interface type */
} rosInterface_t;
/******************************************************/
/*                                                    */
/*                  NODEHANDLE                        */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold information about a nodehandle.
 */
typedef struct rosNodeHandle
{
  /**
   * @brief Advertises a topic to nodehandle
   * @param mh Pointer to messagehandle
   * @param topic Topic name
   * @param buffer_size Buffer size
   * @return Pointer to created publisher
   *
   * Example Usage:
   * @code
   *    // init ROS
   *    rosInit(NULL);
   *    // get pointer to a nodehandle
   *    rosNodeHandle_t* nh = rosNodeHandle("my_nodehandle_interface_id");
   *    // create message
   *    MyMessage_t my_message = rosCreateMyMessage_t(nh);
   *    // advertise message
   *    rosPublisher_t* my_message_pub = nh->advertise(&my_message.mh, "MyMessage", 512);
   *    // publish message
   *    my_message_pub->publish(&my_message);
   * @endcode
   */
  rosPublisher_t* (*advertise)(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size);
  /**
   * @brief Subscribes a topic to a nodehandle
   * @param mh Pointer to messagehandle
   * @param topic Topic name
   * @param buffer_size Buffer size
   * @return Pointer to created subscriber
   *
   * Example Usage:\n\n
   * Subscribers callback function:
   * @code
   *    void myMessageCallback(void *message)
   *    {
   *      // cast message
   *      MyMessage_t *my_message = message;
   *      // Do something here.
   *    }
   * @endcode
   * Program code:
   * @code
   *    // init ROS
   *    rosInit(NULL);
   *    // get pointer to a nodehandle
   *    rosNodeHandle_t* nh = rosNodeHandle("my_nodehandle_interface_id");
   *    // create message
   *    MyMessage_t my_message = rosCreateMyMessage_t(nh);
   *    // advertise message
   *    rosSubscriber_t* my_message_sub = nh->subscribe(&my_message.mh, "MyMessage", 512, myMessageCallback);
   *    // spin ROS continously to process callbacks
   *    rosSpin();
   * @endcode
   */
  rosSubscriber_t* (*subscribe)(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size, void (*callback)(void* message));
  struct rosSpinner* spinner; /**< nodehandle spinner */
  rosInterface_t* interface; /**< device interface */
  rosPublisher_t *publishers[MAX_PUBLISHERS]; /**< array of publishers */
  rosSubscriber_t *subscribers[MAX_SUBSCRIBERS]; /**< array of subscribers */
  uint8_t publisher_count; /**< number of publishers */
  uint8_t subscriber_count; /**< number of subscribers */
  rosReturnCode_t ok; /**< indicates if nodehandle is still alive */
  rosReturnCode_t configured; /**< indicates if topics are already negotiated */
} rosNodeHandle_t;
/******************************************************/
/*                                                    */
/*                  MESSAGEHANDLE                     */
/*                                                    */
/******************************************************/
/**
 * @brief Structure to hold information about message de-/serialization.
 */
typedef struct rosMessageHandle
{
  char* type; /**< message's type */
  char* topic; /**< message's topic */
  char* md5sum; /**< message's MD5 sum */
  uint16_t id; /**< message's id */
  uint16_t buffer_size; /**< message's buffer size */
  rosNodeHandle_t *nodehandle; /**< messagehandle's corresponding nodehandle */
  int (*sizer)(void* message); /**< function pointer to message's sizeof function */
  void (*serializer)(void* message, uint8_t* buffer, int pos); /**< function pointer to message's serializer function */
  void (*deserializer)(void* message, uint8_t* buffer, int pos); /**< function pointer to message's deserializer function */
} rosMessageHandle_t;
/******************************************************/
/*                                                    */
/*                    OS ROS NODES                    */
/*                                                    */
/******************************************************/
/**
 *  @brief Structure to hold information for starting a ROS Node in OS way.
 *
 *    Used to start ROS Nodes in a nice automated way. ;-)
 *
 */
typedef struct osROSNode
{
  void (*node_func)(void* args);
  const char* name;
  uint16_t stack_size;
  uint8_t priority; 
  void* args;
} osROSNode_t;
#endif /* #ifndef __ROS_TYPES_H__ */