/**
 *  @file ros.h
 *  @brief ROS Serial C client library header
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_H__
#define __ROS_H__
/* ROS includes */
#include "ros_std.h"
#include "ros_config.h"
#include "ros_types.h"
#include "ros_time.h"
#include "ros_packet.h"
#include "ros_spinner.h"
#include "ros_hardware.h"
#include "ros_os.h"
/* ROS msgs includes */
#include "TopicInfoC.h"
#include "TimeC.h"
/**
 * @brief Initializes ROS.
 * @param private_namespace the namespace all topics are published or subscribed in
 *
 * Example Usage:
 * @code
 *    // init ROS
 *    rosInit(NULL); // Nothing is prepended befor topic name
 * @endcode
 * or
 * @code
 *    // init ROS
 *    rosInit("my_namespace"); // topics are published as "/my_namespace/topic_name"
 * @endcode
 */
void rosInit(char* private_namespace);
/**
 * @brief Get pointer to global nodehandle.
 * @return pointer to global nodehandle
 */
//rosNodeHandle_t* rosNodeHandle();
rosNodeHandle_t* rosNodeHandle(char* device_id);
/**
 * @brief Checks if nodehandle is still alive.
 * @retval ROS_NOT_OK comparable with false
 * @retval ROS_OK comparable with true
 *
 * Example Usage:
 * @code
 *    while(rosOk())
 *    {
 *      // Do something here.
 *    }
 * @endcode
 */
rosReturnCode_t rosOk(rosNodeHandle_t *nodehandle);
/**
 * @brief Spins global nodehandle continously.
 *
 *  Read continously bytes input buffer and process subscribed callbacks.
 *
 * Example Usage:
 * @code
 *    // init ROS
 *    rosInit(NULL);
 *    // get pointer to global nodehandle
 *    rosNodeHandle_t* nh = rosNodeHandle();
 *    // create message
 *    MyMessage_t my_message = rosCreateMyMessage_t();
 *    // advertise message
 *    rosSubscriber_t* my_message_sub = nh->subscribe(&my_message.messagehandle, "MyMessage", 512, myMessageCallback);
 *    // spin ROS continously to process callbacks
 *    rosSpin();
 * @endcode
 */
//void rosSpin(rosNodeHandle_t *nodehandle);
void rosSpin(rosNodeHandle_t *nodehandle);
/**
 * @brief Spins global nodehandle once.
 *
 *  Reads all current available bytes from input buffer and process subscribed callbacks.
 *
 * Example Usage:
 * @code
 *    // init ROS
 *    rosInit(NULL);
 *    // get pointer to global nodehandle
 *    rosNodeHandle_t* nh = rosNodeHandle();
 *    // create message
 *    MyMessage_t my_message = rosCreateMyMessage_t();
 *    // advertise message
 *    rosSubscriber_t* my_message_sub = nh->subscribe(&my_message.messagehandle, "MyMessage", 512, myMessageCallback);
 *    // enter loop
 *    while(rosOk())
 *    {
 *      // Do something here.
 *
 *      // spin nodehandle once to process callbacks
 *      rosSpinOnce();
 *    }
 * @endcode
 */
void rosSpinOnce(rosNodeHandle_t *nodehandle);
/**
 * @brief Shutdown ROS.
 *
 *  Kills global nodehandle and powers down the board if desired.
 */
void rosShutdown();
/**
 *  @brief Nodehandle publish implementation
 */
rosReturnCode_t rosPublish(void* message);
/**
 *  @brief Nodehandle advertise implementation
 */
rosPublisher_t* rosAdvertise(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size);
/**
 *  @brief Nodehandle subscribe implementation
 */
rosSubscriber_t* rosSubscribe(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size, void (*callback)(void* message));

#endif /* __ROS_H__ */
