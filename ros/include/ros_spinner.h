/**
 *  @file ros_spinner.h
 *  @brief ROS Serial C nodehandle spinner
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_SPINNER_H__
#define __ROS_SPINNER_H__
/* ROS includes */
#include "ros_types.h"
#include "ros_config.h"
/**
 * @brief Structure to hold information about nodehandle's spinner.
 */
struct rosSpinner
{
  uint8_t state; /**< machine state */
  uint8_t last_byte; /**< last received byte */
  uint8_t curr_byte; /**< current received byte */
  uint8_t buffer_initialized; /**< buffer initialized */
  uint16_t counter; /**< message length counter (sync bytes excluded) */
  rosPacket_t packet;
  uint32_t last_sync_time;
  rosReturnCode_t (*spin)(rosNodeHandle_t* nodehandle); /**< spin function */
};
/**
 * @brief Typedef to rosNodeHandle structure
 */
typedef struct rosSpinner rosSpinner_t;

rosReturnCode_t slaveSpinner(rosNodeHandle_t* nodehandle);
rosReturnCode_t masterSpinner(rosNodeHandle_t* nodehandle);

#endif
