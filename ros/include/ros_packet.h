/**
 *  @file ros_packet.h
 *  @brief ROS Serial C packet header
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_PACKET_H__
#define __ROS_PACKET_H__
/* ROS includes */
#include "ros_types.h"
/**
 * @brief Calculate checksum from packet.
 * @param ros_packet Pointer to packet
 * @return Calculated checksum
 */
uint8_t rosPacketChecksum(rosPacket_t* ros_packet);

#endif /* #ifndef __ROS_PACKET_H__ */
