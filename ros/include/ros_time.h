/**
 *  @file ros_time.h
 *  @brief ROS Serial C time
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#ifndef __ROS_TIME_H__
#define __ROS_TIME_H__
/* ROS includes */
#include "ros_types.h"
/**
 * @brief Get current time.
 * @return current time
 */
rosTime_t rosTimeNow();
/**
 * @brief Set current time.
 * @param pointer to a rosTime structure
 */
void rosTimeSetNow(rosTime_t* new_now);
/**
 * @brief Sleep given duration.
 *
 *  Used for achieving a desired timing. If you want to achieve a
 *  continous to use rosRate_t
 */
void rosSleep(uint32_t milliseconds);
/**
 * @brief Initialize rate
 * @param start_time Initial start time
 * @param frequency Frequency to achieve
 * @return None
 */
void rosRateInit(rosRate_t* rate, uint32_t frequency);
/**
 * @brief Sleep given rate.
 * @param rate Pointer to rosRate_t struct
 */
void rosRateSleep(rosRate_t* rate);

#endif /* #ifndef __ROS_TIME_H__ */
