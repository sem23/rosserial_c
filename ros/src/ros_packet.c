/**
 *  @file ros_packet.c
 *  @brief ROS Serial C packet abstraction implementation
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
/* ROS includes */
#include "ros_packet.h"

uint8_t rosPacketChecksum(rosPacket_t* ros_packet)
{
  uint16_t i;
  uint32_t checksum = 0;
  checksum+=ros_packet->topic_id_[0];
  checksum+=ros_packet->topic_id_[1];
  checksum+=ros_packet->message_length_[0];
  checksum+=ros_packet->message_length_[1];
  for (i = 0; i < ros_packet->message_length; ++i)
    checksum+=ros_packet->buffer[i];
  checksum = 255-(checksum%256);
  return checksum;
}
