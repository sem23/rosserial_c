/**
 *  @file ros_spinner.c
 *  @brief ROS Serial C nodehandle spinner implementation
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#include "ros_spinner.h"
#include "ros_hardware.h"

#include "TimeC.h"
#include "TopicInfoC.h"

enum SPINNER_STATES
{
  SYNC = 0,
  TOPIC,
  LENGTH,
  DATA,
  CHECKSUM
};

/* special topic info id's */
enum TOPIC_INFO
{
  ID_PUBLISHER = 0,
  ID_SUBSCRIBER = 1,
  ID_SERVICE_SERVER = 2,
  ID_SERVICE_CLIENT = 4,
  ID_PARAMETER_REQUEST = 6,
  ID_LOG = 7,
  ID_TIME = 10,
};

/* client to host time offsets */
extern uint32_t g_sync_request_time;
extern uint32_t g_sec_offset;
extern uint32_t g_nsec_offset;
/* sync time handling */
extern uint32_t g_last_sync_time;
extern uint32_t g_last_sync_receive_time;
extern uint32_t g_last_msg_timeout_time;

void requestTopicNegotiation(rosNodeHandle_t* nodehandle)
{
  uint8_t i = 0;
  uint8_t message[7] = { 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF };
  for( ; i < 7; ++i)
    nodehandle->interface->send(&message[i]);
}

void responseTopicNegotiation(rosNodeHandle_t *nodehandle)
{
  int i;
  /* setup ROS Topic Info message */
  TopicInfo_t ti = rosCreateTopicInfo_t(nodehandle);
  /* send the topic infos */
  for(i = 0; i < MAX_PUBLISHERS; i++)
  {
    if(nodehandle->publishers[i] != NULL) // non-empty slot
    {
      ti.mh.id = ID_PUBLISHER;
      ti.mh.buffer_size = nodehandle->publishers[i]->messagehandle->buffer_size;
      ti.topic_id = nodehandle->publishers[i]->messagehandle->id;
      ti.topic_name = nodehandle->publishers[i]->messagehandle->topic;
      ti.message_type = nodehandle->publishers[i]->messagehandle->type;
      ti.md5sum = nodehandle->publishers[i]->messagehandle->md5sum;
      ti.buffer_size = nodehandle->publishers[i]->messagehandle->buffer_size;
      rosPublish(&ti);
    }
  }
  for(i = 0; i < MAX_SUBSCRIBERS; i++)
  {
    if(nodehandle->subscribers[i] != NULL) // non-empty slot
    {
      ti.mh.id = ID_SUBSCRIBER;
      ti.mh.buffer_size = nodehandle->subscribers[i]->messagehandle->buffer_size;
      ti.topic_id = nodehandle->subscribers[i]->messagehandle->id;
      ti.topic_name = nodehandle->subscribers[i]->messagehandle->topic;
      ti.message_type = nodehandle->subscribers[i]->messagehandle->type;
      ti.md5sum = nodehandle->subscribers[i]->messagehandle->md5sum;
      ti.buffer_size = nodehandle->subscribers[i]->messagehandle->buffer_size;
      rosPublish(&ti);
    }
  }
  /* set nodehandle in configured state */
  nodehandle->configured = ROS_OK;
}

void requestSyncTime(rosNodeHandle_t* nodehandle)
{
  /* setup ROS Time message */
  Time_t time = rosCreateTime_t(nodehandle);
  time.mh.id = ID_TIME;
  time.mh.buffer_size = 512;
  rosPublish(&time);
  /* set sync reqeuest time */
  g_sync_request_time = hardwareGetTime();
}

void responseSyncTime(rosNodeHandle_t* nodehandle)
{
  /* setup ROS Time message */
  Time_t time = rosCreateTime_t(nodehandle);
  time.mh.id = ID_TIME;
  time.mh.buffer_size = 512;
  /* set data to current ROS time */
  time.data = rosTimeNow();
  rosPublish(&time);
  /* set sync response time */
  nodehandle->spinner->last_sync_time = hardwareGetTime();
}

static void syncTime(void* message)
{
  Time_t t = rosCreateTime_t(NULL);
  t.mh.deserializer(&t, message, 0);
  
  uint32_t offset = hardwareGetTime() - g_sync_request_time;
  t.data.sec += offset/1000;
  t.data.nsec += (offset%1000)*1000000UL;
  rosTime_t time = { t.data.sec, t.data.nsec };
  rosTimeSetNow(&time);
  g_last_sync_receive_time = hardwareGetTime();
  /* toggle sync led */
  hardwareToggleLED(SYNC_LED);
}

rosReturnCode_t slaveSpinner(rosNodeHandle_t* nodehandle)
{
  /* interface & spinner */
  rosInterface_t* interface = nodehandle->interface;
  rosSpinner_t* spinner = nodehandle->spinner;
  /* return code */
  rosReturnCode_t return_code;
  /* get current controller time */
  uint32_t c_time = hardwareGetTime(); 
  /* switch spinner state */  
  switch(spinner->state)
  {
  case SYNC:
  {
    /* receive byte from interface */
    return_code = interface->receive(&spinner->curr_byte);
    /* check if last and current byte ar 0xFF */
    if (spinner->last_byte == (uint8_t)0xFF && 
        spinner->curr_byte == (uint8_t)0xFF)
    {
      /* reset bytes */
      spinner->curr_byte = spinner->last_byte = 0x00;
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = TOPIC;
    }
    else
    {
      /* update last byte */
      spinner->last_byte = spinner->curr_byte;
    }
    break;
  }
  case TOPIC:
  {
    /* set topic id */
    return_code = interface->receive(&spinner->packet.topic_id_[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == 2)
    {
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = LENGTH;
    }
    break;
  }
  case LENGTH:
  {
    /* set message length */
    return_code = interface->receive(&spinner->packet.message_length_[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == 2)
    {
      if(spinner->packet.message_length)
      {
        /* allocate memory once */
        if(!spinner->buffer_initialized)
        {
          spinner->packet.buffer = malloc(MIN_PACKET_SIZE);
          spinner->packet.buffer_size = MIN_PACKET_SIZE;
          spinner->buffer_initialized = 1;
        }
        /* reallocate memory if message length exceeds buffersize */
        if(spinner->packet.message_length > spinner->packet.buffer_size)
        {
          realloc(spinner->packet.buffer, spinner->packet.message_length);
          spinner->packet.buffer_size = spinner->packet.message_length;
        }
        /* update state */
        spinner->state = DATA;
        /* reset counter */
        spinner->counter = 0;
      }
      else
      {
        /* update state */
        spinner->state = CHECKSUM;
      }
    }
    break;
  }
  case DATA:
  {
    /* read data to allocated memory */
    return_code = interface->receive(&spinner->packet.buffer[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == spinner->packet.message_length)
    {
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = CHECKSUM;
    }
    break;
  }
  case CHECKSUM:
  {
    /* read checksum */
    return_code = interface->receive(&spinner->packet.checksum);

    if (spinner->packet.checksum == rosPacketChecksum(&spinner->packet))
    {
      if(spinner->packet.topic_id == ID_PUBLISHER)
      {
        requestSyncTime(nodehandle);
        responseTopicNegotiation(nodehandle);
        g_last_sync_time = c_time;
        g_last_sync_receive_time = c_time;
      }
      else if(spinner->packet.topic_id == ID_TIME)
      {
        syncTime(spinner->packet.buffer);
      }
      else if (spinner->packet.topic_id == ID_PARAMETER_REQUEST)
      {
//          req_param_resp.deserialize(message_in);
//          param_recieved= true;
      }
      else
      {
        rosSubscriber_t* sub = nodehandle->subscribers[spinner->packet.topic_id - 100];
        /* execute callback on existance */
        if(sub != NULL)
        {
          /* execute callback */
          sub->callback(spinner->packet.buffer);
        }
      }
    }
    else
    {
      hardwareToggleLED(ERROR_LED);
    }
    /* reset counter */
    spinner->counter = 0;
    /* update state */
    spinner->state = SYNC;
    break;
  }
  /* go back to sync state at default */
  default:
    /* reset counter */
    spinner->counter = 0;
    /* update state */
    spinner->state = SYNC;
    break;
  }

  /* occasionally sync time */
  if( nodehandle->configured == ROS_OK && 
    ( (c_time - g_last_sync_time) > (SYNC_SECONDS * 1000) ))
  {
    requestSyncTime(nodehandle);
    g_last_sync_time = c_time;
  }
  /* return return_code */
  return return_code;
}

rosReturnCode_t masterSpinner(rosNodeHandle_t* nodehandle)
{
  /* interface & spinner */
  rosInterface_t* interface = nodehandle->interface;
  rosSpinner_t* spinner = nodehandle->spinner;
  /* return code */
  rosReturnCode_t return_code;
  /* get current controller time */
  uint32_t c_time = hardwareGetTime(); 
  /* switch spinner state */
  switch(spinner->state)
  {
  case SYNC:
  {
    /* receive byte from interface */
    return_code = interface->receive(&spinner->curr_byte);
    /* check if last and current byte ar 0xFF */
    if (spinner->last_byte == (uint8_t)0xFF && 
        spinner->curr_byte == (uint8_t)0xFF)
    {
      /* reset bytes */
      spinner->curr_byte = spinner->last_byte = 0x00;
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = TOPIC;
    }
    else
    {
      /* update last byte */
      spinner->last_byte = spinner->curr_byte;
    }
    break;
  }
  case TOPIC:
  {
    /* set topic id */
    return_code = interface->receive(&spinner->packet.topic_id_[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == 2)
    {
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = LENGTH;
    }
    break;
  }
  case LENGTH:
  {
    /* set message length */
    return_code = interface->receive(&spinner->packet.message_length_[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == 2)
    {
      if(spinner->packet.message_length)
      {
        /* allocate memory once */
        if(!spinner->buffer_initialized)
        {
          spinner->packet.buffer = malloc(MIN_PACKET_SIZE);
          spinner->packet.buffer_size = MIN_PACKET_SIZE;
          spinner->buffer_initialized = 1;
        }
        /* reallocate memory if message length exceeds buffersize */
        if(spinner->packet.message_length > spinner->packet.buffer_size)
        {
          realloc(spinner->packet.buffer, spinner->packet.message_length);
          spinner->packet.buffer_size = spinner->packet.message_length;
        }
        /* update state */
        spinner->state = DATA;
        /* reset counter */
        spinner->counter = 0;
      }
      else
      {
        /* update state */
        spinner->state = CHECKSUM;
      }
    }
    break;
  }
  case DATA:
  {
    /* read data to allocated memory */
    return_code = interface->receive(&spinner->packet.buffer[spinner->counter]);
    /* increment counter */
    if(++spinner->counter == spinner->packet.message_length)
    {
      /* reset counter */
      spinner->counter = 0;
      /* update state */
      spinner->state = CHECKSUM;
    }
    break;
  }
  case CHECKSUM:
  {
    /* read checksum */
    return_code = interface->receive(&spinner->packet.checksum);
    /* proof checksum */
    if (spinner->packet.checksum == rosPacketChecksum(&spinner->packet))
    {
      if(spinner->packet.topic_id == ID_PUBLISHER) /* topic negotiation */
      {
      }
      else if(spinner->packet.topic_id == ID_SUBSCRIBER) /* topic negotiation */
      {
      }
      else if(spinner->packet.topic_id == ID_TIME)
      {
        /* response sync time request */
        responseSyncTime(nodehandle);
        /* set nodehandle in configured state */
        nodehandle->configured = ROS_OK;
      }
      else /* execute callback on existance */
      {
        if(nodehandle->subscribers[spinner->packet.topic_id-100] != NULL)
        {
          nodehandle->subscribers[spinner->packet.topic_id-100]->callback( spinner->packet.buffer );
        }
      }
    }
    else
    {
      hardwareToggleLED(ERROR_LED);
    }
    /* reset counter */
    spinner->counter = 0;
    /* update state */
    spinner->state = SYNC;
    break;
  }
  /* go back to sync state at default */
  default:
    /* reset counter */
    spinner->counter = 0;
    /* update state */
    spinner->state = SYNC;
    break;
  }
  /* check sync */
  if((c_time - spinner->last_sync_time) > (SYNC_SECONDS * 1000))
  {
    /* request topic negotiation */
    requestTopicNegotiation(nodehandle);
    /* set nodehandle in un-configured state */
    nodehandle->configured = ROS_NODEHANDLE_NOT_CONFIGURED;
  }
  /* return return_code */
  return return_code;
}
