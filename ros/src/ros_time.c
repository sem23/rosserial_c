/**
 *  @file ros_time.c
 *  @brief ROS Serial C time implementation
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#include "ros_time.h"
#include "ros_os.h"

/* controller to host time offsets */
uint32_t g_sync_request_time = 0;
uint32_t g_sec_offset = 0;
uint32_t g_nsec_offset = 0;
/* sync time handling */
uint32_t g_last_sync_time = 0;
uint32_t g_last_sync_receive_time = 0;
uint32_t g_last_msg_timeout_time = 0;

static void normalizeSecNSec(uint32_t* sec, uint32_t* nsec)
{
  uint32_t nsec_part= *nsec % 1000000000UL;
  uint32_t sec_part = *nsec / 1000000000UL;
  *sec += sec_part;
  *nsec = nsec_part;
}

rosTime_t rosTimeNow()
{
  uint32_t ms = hardwareGetTime();
  rosTime_t current_time;
  current_time.sec = ms/1000 + g_sec_offset;
  current_time.nsec = (ms%1000)*1000000UL + g_nsec_offset;
  normalizeSecNSec(&current_time.sec, &current_time.nsec);
  return current_time;
}

void rosTimeSetNow(rosTime_t* new_now)
{
  uint32_t ms = hardwareGetTime();
  g_sec_offset = new_now->sec - ms/1000 - 1;
  g_nsec_offset = new_now->nsec - (ms%1000)*1000000UL + 1000000000UL;
  normalizeSecNSec(&g_sec_offset, &g_nsec_offset);
}

void rosRateInit(rosRate_t* rate, uint32_t frequency)
{
  rate->last_wake_time = hardwareGetTime();
  rate->frequency = frequency;
}

void rosRateSleep(rosRate_t* rate)
{
#ifdef ROS_USE_OS
  osDelayUntil(&rate->last_wake_time, (hardwareTICK_RATE_HZ / rate->frequency));
#else
#warning "This is a blocking function call! Only use in single node mcu's! Otherwise use OS implementation!"
  uint32_t start = rate->last_wake_time;
  while( hardwareGetTime() - start < (hardwareTICK_RATE_HZ / rate->frequency)){;}
  rate->last_wake_time = hardwareGetTime();
#endif
}
