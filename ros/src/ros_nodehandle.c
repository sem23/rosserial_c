/**
 *  @file ros_nodehandle.c
 *  @brief ROS Serial C nodehandle implementation
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
/* ROS includes */
#include "ros.h"

/* special topic info id's */
enum TOPIC_INFO
{
  ID_PUBLISHER = 0,
  ID_SUBSCRIBER = 1,
  ID_SERVICE_SERVER = 2,
  ID_SERVICE_CLIENT = 4,
  ID_PARAMETER_REQUEST = 6,
  ID_LOG = 7,
  ID_TIME = 10,
};

rosReturnCode_t rosPublish(void* message)
{
  /* buffer initialized */
  static uint8_t buffer_initialized = 0;
  /* the packet we send */
  static rosPacket_t ros_packet;
  /* due to knowing that every message's first element is a */
  /* message handle we cast every message as message handle */
  /* ----------- this is a legal pointer cast! ------------ */
  rosMessageHandle_t* messagehandle = (rosMessageHandle_t*) message;
  /* get nodehandle & interface*/
  rosNodeHandle_t* nh = messagehandle->nodehandle;
  rosInterface_t* interface = nh->interface;
  /* return if nodehandle is not configured or lock system  */
  /* due interface transmission must not be interrupted!!   */
  if(messagehandle->id >= 100 && nh->configured != ROS_OK)
    return ROS_NODEHANDLE_NOT_CONFIGURED;
#ifdef ROS_USE_OS
  osLockCritical();
  hardwareToggleLED(LOCK_LED);
#endif
  /* set sync flag */
  ros_packet.sync_flag      = 0xFFFF;
  /* set topic id */
  ros_packet.topic_id       = messagehandle->id;
  /* get message length */
  ros_packet.message_length = messagehandle->sizer(message);
  /* allocate memory once */
  if(!buffer_initialized)
    ros_packet.buffer         = malloc(messagehandle->buffer_size);
  buffer_initialized = 1;
  /* reallocate memory if message length exceeds buffersize */
  if(ros_packet.message_length > ros_packet.buffer_size)
    ros_packet.buffer = realloc(ros_packet.buffer, ros_packet.message_length);
  /* init checksum */
  ros_packet.checksum       = 0xFF;
  /* serialize message to allocated data array */
  messagehandle->serializer(message, ros_packet.buffer, 0);
  /* send packet */
  uint16_t i;
  uint8_t checksum;
  /* send message */
  interface->send(&ros_packet.sync_flag_[0]);
  interface->send(&ros_packet.sync_flag_[1]);
  interface->send(&ros_packet.topic_id_[0]);
  interface->send(&ros_packet.topic_id_[1]);
  interface->send(&ros_packet.message_length_[0]);
  interface->send(&ros_packet.message_length_[1]);
  for (i = 0; i < ros_packet.message_length; ++i)
    interface->send(&ros_packet.buffer[i]);
  checksum = rosPacketChecksum(&ros_packet);
  interface->send(&checksum);
#ifdef ROS_USE_OS
  /* unlock operating system */
  osUnlockCritical();
  hardwareToggleLED(LOCK_LED);
#endif
}

rosReturnCode_t rosPublishLoopback(void* message)
{
  /* due to knowing that every message's first element is a */
  /* message handle we cast every message as message handle */
  /* ----------- this is a legal pointer cast! ------------ */
  rosMessageHandle_t* messagehandle = (rosMessageHandle_t*) message;
  /* return if nodehandle is not configured */
  if(messagehandle->id >= 100 && messagehandle->nodehandle->configured != ROS_OK)
    return ROS_NODEHANDLE_NOT_CONFIGURED;
  /* iterate through nodehandles subscribers */
  int i;
  for(i = 0; i < messagehandle->nodehandle->subscriber_count; ++i)
  {
    /* get subscriber */
    rosSubscriber_t* subscriber = messagehandle->nodehandle->subscribers[i];
    /* check if topic exists */
    if(strcmp(messagehandle->topic, subscriber->messagehandle->topic) == 0)
    {
      /* execute callback on topic match */
      subscriber->callback(message);
    }
  }
}

rosPublisher_t* rosAdvertise(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size)
{
  /* set topic name */
  messagehandle->topic = topic;
  /* generate topic id */
  messagehandle->id = PUBLISHER_TOPICS_BEGIN + messagehandle->nodehandle->publisher_count;
  /* set buffer size */
  messagehandle->buffer_size = buffer_size;
  /* create publisher */
  rosPublisher_t *publisher = malloc(sizeof(rosPublisher_t));
  publisher->messagehandle = messagehandle;
  if(messagehandle->nodehandle->interface->interface_type == LOOPBACK)
    publisher->publish = rosPublishLoopback;
  else
    publisher->publish = rosPublish;
  /* add publisher to messagehandle's nodehandle */
  messagehandle->nodehandle->publishers[messagehandle->nodehandle->publisher_count] = publisher;
  /* increment publishers count */
  messagehandle->nodehandle->publisher_count++;
  return publisher;
}

rosSubscriber_t* rosSubscribe(rosMessageHandle_t* messagehandle, char* topic, uint16_t buffer_size, void (*callback)(void* message))
{
  /* set topic name */
  messagehandle->topic = topic;
  /* generate topic id */
  messagehandle->id = SUBSCRIBER_TOPICS_BEGIN + messagehandle->nodehandle->subscriber_count;
  /* set buffer size */
  messagehandle->buffer_size = buffer_size;
  /* create subscriber */
  rosSubscriber_t *subscriber = malloc(sizeof(rosSubscriber_t));
  subscriber->messagehandle = messagehandle;
  subscriber->callback = callback;
  /* add subscriber to messagehandle's nodehandle */
  messagehandle->nodehandle->subscribers[messagehandle->nodehandle->subscriber_count] = subscriber;
  /* increment subscribers count */
  messagehandle->nodehandle->subscriber_count++;
  return subscriber;
}

/* make global nodehandles availbale */
extern rosNodeHandle_t* g_nodehandles[];
extern int g_nodehandles_size;

rosNodeHandle_t* rosNodeHandle(char * device_id)
{
  /* iterate through all nodehandles */
  int i;
  for(i = 0; i < g_nodehandles_size; ++i)
  {
    if(g_nodehandles[i] != NULL)
    {
      char* id = g_nodehandles[i]->interface->device_id;
      /* check if nodehandle with correspondig interface device id exists */
      if(strcasecmp(id, device_id) == 0)
        return g_nodehandles[i];
    }
  }
  /* return NULL if no nodehandle was found */
  return NULL;
}

void rosSpinAll()
{
  /* iterate through all nodehandles */
  int i;
  for(i = 0; i < g_nodehandles_size; ++i)
  {
    if(g_nodehandles[i] != NULL)
    {
      rosSpinOnce(g_nodehandles[i]);
    }
  }
}
