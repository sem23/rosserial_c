/**
 *  @file ros.c
 *  @brief ROS Serial C library implementation
 *  @date 04/19/2013
 *  @author Peter Rudolph
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n
 *
 *  @copyright Peter Rudolph, 2013. All rights reserved.
 *  @license This library is released under <a href="http://www.gnu.org/licenses/lgpl-3.0.de.html">LGPLv3</a>.
 */
#include "ros.h"

char* g_namespace;

void rosInit(char * private_namespace)
{
  /* set hardware namespace */
  g_namespace = private_namespace;
  /* call hardware initializer */
  hardwareInit();
  /* OS specifics */
#ifdef ROS_USE_OS
  /* start predefined nodes */
#ifdef ROS_START_NODES_WITH_OS
  uint8_t i = 0;
  extern osROSNode_t g_nodes[];
  extern uint8_t g_nodes_size;
  /* starter loop */
  for(; i < g_nodes_size; ++i)
    osStartNode(&g_nodes[i]);
#endif /* #ifdef ROS_START_NODES_WITH_OS */
  /* call OS initializer */
  osInit();
#endif /* #ifdef ROS_USE_OS */
}

rosReturnCode_t rosOk(rosNodeHandle_t* nodehandle)
{
  return nodehandle->ok;
}

void rosSpin(rosNodeHandle_t *nodehandle)
{
  while(rosOk(nodehandle))
  {
    /* call state machine */
    nodehandle->spinner->spin(nodehandle);
    /* toggle spin led */
    hardwareToggleLED(SPIN_LED);
  }
}

void rosSpinOnce(rosNodeHandle_t *nodehandle)
{
  /* toggle spin led */
  hardwareToggleLED(SPIN_LED);
  
  /* call state machine */
  while(nodehandle->spinner->spin(nodehandle) != ROS_FIFO_BUFFER_EMPTY)
  {
    
  }
}

void rosSleep(uint32_t milliseconds)
{
#ifdef ROS_USE_OS
  osMSleep(milliseconds);
#else
  uint32_t start = hardwareGetTime();
  while( hardwareGetTime() - start < milliseconds){;}
#endif
}

void rosShutdown()
{

}

